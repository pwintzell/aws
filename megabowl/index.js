var express = require('express');
var http = require('http');
var os = require('os');
var path = require('path');

var app = express();


var courses = [
	{
		name: "Onsala 1"
	},
	{
		name: "Onsala 2"
	}
];

var players_reset = [
	{
		name: "Seb",
		hcp: 0,
		score: {
			"Onsala 1": 0,
			"Onsala 2": 0
		}
	},
	{
		name: "Mack",
		hcp: 1,
		score: {
			"Onsala 1": 0,
			"Onsala 2": 0
		}
	},
	{
		name: "Gurra",
		hcp: 6,
		score: {
			"Onsala 1": 0,
			"Onsala 2": 0
		}
	},
	{
		name: "Wolf",
		hcp: 8,
		score: {
			"Onsala 1": 0,
			"Onsala 2": 0
		}
	},
	{
		name: "P",
		hcp: 9,
		score: {
			"Onsala 1": 0,
			"Onsala 2": 0
		}
	}
];

var messages = [];
var players = JSON.parse(JSON.stringify(players_reset));
var openConnections = [];
var currentCourse = 0;

var sendNewScore = function(playerName, courseName, newScore){
	for (var j = 0; j < players.length ; j++) {
		if (players[j].name === playerName){
			players[j].score[courseName] = parseInt(newScore);
			break;
		}
	}
	var data = {
		'type' : 'newScore',
		'playerName' : playerName,
		'courseName' : courseName,
		'newScore' : newScore,
		'currentCourse' : currentCourse
	}
	sendInfo(data);
}

var sendHoleInOneNotification = function(playerName) {
	sendInfo({
		'type' : 'holeInOne',
		'playerName' : playerName
	});
}

var sendAllData = function() {
	var data = {
		type: "updateAll",
		courses: courses,
		players: players,
		messages: messages,
		currentCourse: currentCourse
	}
	sendInfo(data);
}

var sendChatMessage = function(playerName, message) {
	if (message === "/full-reset"){
		players = JSON.parse(JSON.stringify(players_reset));
		console.log(players);
		currentCourse = 0;
		sendAllData();
		return;
	}
	if (message === "/next"){
		if (currentCourse < courses.length-1){
			currentCourse++;
			sendAllData();
		}
		return;
	}
	if (message === "/previous" || message === "/prev"){
		if (currentCourse > 0){
			currentCourse--;
			sendAllData();
		}
		return;
	}

	var messageItem = {
		'type' : 'chatMessage',
		'playerName' : playerName,
		'time' : Date.now(),
		'message' : message
	};
	sendInfo(messageItem);
	messages.unshift(messageItem);
	if (messages.length > 40){
		messages.pop();
	}
}

var sendInfo = function(data){
    openConnections.forEach(function(resp) {
    	resp.write('event: message' + '\n');
        resp.write('data: ' + JSON.stringify(data) + '\n\n');
    });
}

app.get('/ajax/push-update', function(req, res) {
 
    // set timeout as high as possible
    req.socket.setTimeout(0x7FFFFFFF);
 
    // send headers for event-stream connection
    // see spec for more information
    res.writeHead(200, {
        'Content-Type': 'text/event-stream',
        'Cache-Control': 'no-cache',
        'Connection': 'keep-alive'
    });
    res.write('\n');
 
    // push this res object to our global variable
    openConnections.push(res);
 
    // When the request is closed, e.g. the browser window
    // is closed. We search through the open connections
    // array and remove this connection.
    req.on("close", function() {
        var toRemove;
        for (var j =0 ; j < openConnections.length ; j++) {
            if (openConnections[j] == res) {
                toRemove =j;
                break;
            }
        }
        openConnections.splice(toRemove,1);
    });
});




app.get('/ajax/update', function(request, response){
	response.send("\n")
	if (request.query.type === "holeInOne"){
		sendHoleInOneNotification(request.query.playerName);
		sendChatMessage(request.query.playerName, "HIO");
	} else if (request.query.type === "chatMessage"){
		sendChatMessage(request.query.playerName, request.query.message);
	} else {
		sendNewScore(request.query.playerName, request.query.courseName, request.query.newScore);
	}
});

app.get('/ajax/get-all', function(request, response){
	var data = {
		courses: courses,
		players: players,
		messages: messages,
		currentCourse: currentCourse
	}
	response.send(data);
});

app.use('/', express.static(__dirname + '/public'));

app.listen(501, function() {
	console.log('Starting server');
});
