angular.module("scoreModule", []).
filter("scoreBoardSorter", function() {
	return function(items, field) {
		var filtered = [];
		angular.forEach(items, function(item) {
			filtered.push(item);
		});
		filtered.sort(function (a, b) {    
			if (a.currentScore === b.currentScore){
				return a.hcp > b.hcp ?  1 : -1;
			}
			return a.currentScore > b.currentScore ? 1 : -1;
		});
		return filtered;
	};
}).
controller("mainController", ["$scope", "$http", "$animate", function ($scope, $http, $animate) {

	var USER_KEY_NAME = "megabowl-selected-user";

	var updateScore = function(playerName, courseName, newScore) {
		var params = {
			'type' : 'newScore',
			'playerName' : playerName,
			'courseName' : courseName,
			'newScore' : newScore
		}
		sendMessage(params);
	}

	var sendMessage = function(params){
		var data = {
			params : params
		}
		$http.get('/ajax/update', data).success(function() {
		});	
	}

	$scope.sendChatMessage = function(){
		if ($scope.chatMessage === "/reset"){
			localStorage.removeItem(USER_KEY_NAME); 
			$scope.chatExpanded = false;
			$scope.chatMessage = "";
			$scope.authenticated = false;
			$scope.user = undefined;
			return;
		}
		var params = {
			'type' : 'chatMessage',
			'playerName' : $scope.user,
			'message' : $scope.chatMessage
		} 
		sendMessage(params);
		$scope.chatMessage = "";
	}

	$scope.authenticate = function(){
		if ($scope.user === undefined){
			return;
		}
		localStorage.setItem(USER_KEY_NAME, $scope.user); 
		$scope.authenticated = true;
	}

	$scope.holeInOne = function(){
		$scope.holeInOneConfirmed = false;
		sendMessage({
			'type' : 'holeInOne',
			'playerName' : this.player.name
		})
	}

	$scope.calculateCurrentScore = function(player){
		var tot = -player.hcp*($scope.currentUserCourse +1);
		var index = 0;
		angular.forEach(player.score, function(value){
			if (index <= $scope.currentUserCourse){
				tot += value;
			}
			index++;
		})
		return tot;
	}	

	$scope.calculateTotalScore = function(player){
		var tot = -player.hcp*$scope.courses.length;
		angular.forEach(player.score, function(value){
			tot += value;
		})
		return tot;
	}

	$scope.selectNextCourse = function(){
		if ($scope.currentUserCourse + 1 >= $scope.courses.length){
			return;
		}
		$scope.currentUserCourse++;
		$scope.calcPlayerScores();
	}
	$scope.selectPreviousCourse = function(){
		if ($scope.currentUserCourse <= 0){
			return;
		}
		$scope.currentUserCourse--;
		$scope.calcPlayerScores();
	}

	$scope.increase = function(){
		this.player.change = 'increase';
		this.player.score[$scope.courses[$scope.currentUserCourse].name] += 1;
		this.player.totalScore += 1;
		this.player.currentScore += 1;
		updateScore(this.player.name, $scope.courses[$scope.currentUserCourse].name, this.player.score[$scope.courses[$scope.currentUserCourse].name]);
	}
	$scope.decrease = function(){
		this.player.change = 'decrease';
		this.player.score[$scope.courses[$scope.currentUserCourse].name] -= 1;
		this.player.totalScore -= 1;
		this.player.currentScore -= 1;
		updateScore(this.player.name, $scope.courses[$scope.currentUserCourse].name, this.player.score[$scope.courses[$scope.currentUserCourse].name]);
	}
	$scope.calcPlayerScores = function(){
		angular.forEach($scope.players, function(player){
			player.totalScore = $scope.calculateTotalScore(player);
			player.currentScore = $scope.calculateCurrentScore(player);
		});
	}
	$scope.setAllData = function(data){
		$scope.courses = data.courses;
		$scope.players = data.players;
		$scope.chatMessages = data.messages;
		$scope.currentCourse = data.currentCourse;
		$scope.currentUserCourse = data.currentCourse;
		$scope.calcPlayerScores();
	}


	$http.get('/ajax/get-all').success(function(data) {
		$scope.setAllData(data);
	});

    var source = new EventSource('/ajax/push-update');
    source.addEventListener('message', function(event) {
    	$scope.$apply(function () {
    		var params = JSON.parse(event.data)
    		if (params.type === 'holeInOne'){
    			$scope.holeInOneConfirmed = true;
    			$scope.holeInOnePlayerName = params.playerName;
    		} else if (params.type === 'chatMessage'){
    			var newMessage = {
    				'playerName' : params.playerName,
    				'time' : params['time'],
    				'message' : params.message
    			}
    			$scope.chatMessages.unshift(newMessage);
			} else if (params.type === 'updateAll'){
				$scope.setAllData(params);
    		} else {
				for (var j = 0; j < $scope.players.length ; j++) {
					if ($scope.players[j].name === params.playerName){
						var eval = $scope.players[j].score[params.courseName] - parseInt(params.newScore);
						if (eval > 0){
							$scope.players[j].change = 'decrease';
						} else if (eval < 0){
							$scope.players[j].change = 'increase';
						}
						$scope.players[j].score[params.courseName] = parseInt(params.newScore);
						setTimeout(function() {
		                	$scope.$apply(function() {
		                    	$scope.players[j].change = 'none';
		                  	});
		              	}, 1000);
						break;
					}
				}
				$scope.calcPlayerScores();
			}
		});
	}, false);
	
	var userFromStorage = localStorage.getItem(USER_KEY_NAME);
	if (userFromStorage){
		$scope.user = userFromStorage;
		$scope.authenticated = true;
	}
}]).
directive('ngConfirmClick', [
    function(){
        return {
            link: function ($scope, $element, $attr) {
                var msg = $attr.ngConfirmClick || "Really though?";
                var clickAction = $attr.confirmedClick;
                $element.bind('click',function (event) {
                    if ( window.confirm(msg) ) {
                    	$scope.$eval(clickAction)
                    }
                });
            }
        };
}]);


(function() {
    var lastTouch = 0;
    function preventZoom(e) {
        var t2 = e.timeStamp;
        var t1 = lastTouch || t2;
        var dt = t2 - t1;
        var fingers = e.touches.length;
        lastTouch = t2;

        if (!dt || dt >= 300 || fingers > 1) {
            return;
        }
        resetPreventZoom();
        e.preventDefault();
        e.target.click();
    }
    function resetPreventZoom() {
        lastTouch = 0;
    }

    document.addEventListener('touchstart', preventZoom, false);
    document.addEventListener('touchmove', resetPreventZoom, false);
})();

