import { useEffect, useState } from "react";
import { targetDate, testTargetDate } from "../../server/setup-data";

// Temp
// targetDate.setTime(new Date().getTime() + 1000 * 1);

function Countdown({ onTime }) {
  const [string, setString] = useState("");

  useEffect(() => {
    function setCountdownString() {
      const useTest = window.location.search.startsWith("?trump=");
      const now = new Date().getTime();
      let target = (useTest ? testTargetDate : targetDate).getTime();

      if (now >= target) {
        onTime();
        clearInterval(interval);
        return;
      }

      const days = Math.floor((target - now) / (24 * 60 * 60 * 1000));
      target -= days * (24 * 60 * 60 * 1000);

      const hours = Math.floor((target - now) / (60 * 60 * 1000));
      target -= hours * (60 * 60 * 1000);

      const minutes = Math.floor((target - now) / (60 * 1000));
      target -= minutes * (60 * 1000);

      const seconds = Math.floor((target - now) / 1000);

      let countdownString = "";
      if (days > 0) {
        countdownString =
          addPad(days) +
          ":" +
          addPad(hours) +
          ":" +
          addPad(minutes) +
          ":" +
          addPad(seconds);
      } else {
        countdownString =
          addPad(hours) + ":" + addPad(minutes) + ":" + addPad(seconds);
      }

      setString(countdownString);
    }

    const interval = setInterval(setCountdownString, 1000);
    setCountdownString();

    return () => {
      clearInterval(interval);
    };
  }, [onTime]);

  return string;
}

function addPad(value) {
  if (value > 9) {
    return value;
  }
  return "0" + value;
}

export default Countdown;
