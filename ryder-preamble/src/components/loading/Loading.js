import React from 'react';
import './Loading.css';

class Loading extends React.Component {
  render() {
    return (
      <div className="loading">
      	<p>Loading...</p>
      </div>  
    )
  }
}

export default Loading;