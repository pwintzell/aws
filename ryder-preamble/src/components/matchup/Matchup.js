import React, { useMemo } from "react";
import "./Matchup.css";

function Matchup({ players, teams }) {
  const { usa, eur } = teams;
  const playersDictionary = useMemo(
    () =>
      players.reduce((acc, player) => {
        acc[player.id] = player;
        return acc;
      }, {}),
    [players]
  );

  return (
    <div className="matchup_container">
      <div className="matchup_logo-top">
        <img src="img/logo.png" className="matchup_logo-top-image" alt="" />
      </div>
      <div className="matchup">
        <div className="matchup_team" style={{ textAlign: "left" }}>
          {usa.map((playerId, index) => {
            const player = playersDictionary[playerId];
            const { firstName, lastName } = player;
            return (
              <div
                key={playerId}
                className="matchup_player"
                style={{ animationDelay: index * 10 + "ms" }}
              >
                <div
                  className="matchup_player-logo"
                  style={{
                    backgroundImage: "url(img/usa_board.png)",
                  }}
                >
                  <img
                    src={getImageUrl(player)}
                    className="matchup_player-img"
                    alt=""
                  />
                </div>
                <div className="matchup_player-name">
                  {firstName} {lastName}
                </div>
                <div className="matchup_player-name matchup_player-name--alt">
                  {firstName.substring(0, 1)}. {lastName}
                </div>
              </div>
            );
          })}
        </div>
        <div className="matchup_logo">
          <img src="img/logo.png" alt="" />
        </div>
        <div className="matchup_team" style={{ textAlign: "right" }}>
          {eur.map((playerId, index) => {
            const player = playersDictionary[playerId];
            const { firstName, lastName } = player;
            return (
              <div
                key={playerId}
                className="matchup_player"
                style={{
                  justifyContent: "flex-end",
                  animationDelay: index * 10 + "ms",
                }}
              >
                <div className="matchup_player-name">
                  {firstName} {lastName}
                </div>
                <div
                  className="matchup_player-logo"
                  style={{
                    backgroundImage: "url(img/eur_board.png)",
                    backgroundPositionX: "38px",
                    backgroundPositionY: "54px",
                  }}
                >
                  <img
                    src={getImageUrl(player)}
                    className="matchup_player-img"
                    alt=""
                  />
                </div>
                <div className="matchup_player-name matchup_player-name--alt">
                  {firstName.substring(0, 1)}. {lastName}
                </div>
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
}

function getImageUrl(player) {
  const url = `img/${player.firstName}_${player.lastName}.png`;
  return translateToUrlFriendlyName(url).toLowerCase();
}

function translateToUrlFriendlyName(string) {
  return string
    .replace(/[åä]{1}/g, "a")
    .replace(/[ÅÄ]{1}/g, "A")
    .replace(/[ö]{1}/g, "o")
    .replace(/[Ö]{1}/g, "O");
}

export default Matchup;
