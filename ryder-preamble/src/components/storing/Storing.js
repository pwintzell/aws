import React from 'react'
import './Storing.css'

const Storing = ({clearMatchupHoleScore, saveMatchupHoleScore}) => {
  return (
    <div className='scoring_storing--down'>
      <button className='scoring_storage-button' onClick={clearMatchupHoleScore}>CLEAR</button>
      <button className='scoring_storage-button' onClick={saveMatchupHoleScore}>SAVE</button>
    </div>
  )
}

export default Storing
