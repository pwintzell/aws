import React from 'react'
import './StrokeHandling.css'

const StrokeHandling = ({initials, score, color, addStroke, subtractStroke}) => {
  let classNames = 'scoring_stroke-handling scoring_stroke-handling--' + color
  return (
    <div className={classNames}>
      <span className='scoring_initials'>{initials}</span>
      <button className='scoring_change-strokes' onClick={addStroke}>+</button>
      <span className='scoring_strokes'>{score}</span>
      <button className='scoring_change-strokes' onClick={subtractStroke}>-</button>
    </div>
  )
}

export default StrokeHandling
