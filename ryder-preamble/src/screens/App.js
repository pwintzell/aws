import React, { useState } from "react";
import "./App.css";
import Countdown from "../components/countdown/Countdown";
import Matchup from "../components/matchup/Matchup";

function App() {
  const [teams, setTeams] = useState(null);
  const [players, setPlayers] = useState(null);

  async function loadTeams() {
    const response = await fetch("/ajax/get-teams" + getCode(), {
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json",
      },
    });
    const { teams, players } = await response.json();
    setTeams(teams);
    setPlayers(players);
  }

  return (
    <div className="app">
      <div
        className="flagContainer"
        style={{
          backgroundImage: "url(img/usa_board.png)",
          backgroundPositionX: "center",
        }}
      />
      <div className="content">
        {!teams || !players ? (
          <h1>
            Teams will be presented
            <br />
            4th of May 9:00 P.M.
            <br />
            <br />
            <Countdown onTime={loadTeams} />
          </h1>
        ) : (
          <Matchup players={players} teams={teams} />
        )}
      </div>
      <div
        className="flagContainer"
        style={{
          backgroundImage: "url(img/eur_board.png)",
          backgroundPositionX: "center",
        }}
      />
    </div>
  );
}

function getCode() {
  const useTest = window.location.search.startsWith("?trump=");
  if (useTest) {
    return window.location.search;
  }
  return "";
}

export default App;
