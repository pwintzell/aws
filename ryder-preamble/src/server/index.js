const express = require("express");
const bodyParser = require("body-parser");
import { targetDate, testTargetDate, players } from "./setup-data.js";
import { findMostEvenTeams } from "./teams-calc.js";

let app = (module.exports = express());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
const runbyCode = "solid-flu-vaccine";

console.log("Running ryder preamble!");

app.get("/ajax/get-teams", function (request, response) {
  console.log("Get teams");
  const code = request.query.trump;
  const target = code === runbyCode ? testTargetDate : targetDate;
  const now = new Date();
  if (now.getTime() > target.getTime() - 5 * 1000) {
    response.send({
      players,
      teams: findMostEvenTeams(),
    });
  } else {
    response.send({ message: "Patience nerd" });
  }
});

app.use("/", express.static("build"));
app.listen(502);
