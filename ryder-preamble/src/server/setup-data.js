const targetDate = new Date();
targetDate.setUTCFullYear(2020);
targetDate.setUTCMonth(4);
targetDate.setUTCDate(4);
targetDate.setUTCHours(19);
targetDate.setUTCMinutes(0);
targetDate.setUTCSeconds(0);
targetDate.setMilliseconds(0);

// TEMP
/* targetDate.setUTCFullYear(2020);
targetDate.setUTCMonth(4);
targetDate.setUTCDate(4);
targetDate.setUTCHours(10);
targetDate.setUTCMinutes(45);
targetDate.setUTCSeconds(0);
targetDate.setMilliseconds(0); */

const testTargetDate = new Date();
testTargetDate.setTime(new Date().getTime() + 1000 * 3);

export { targetDate, testTargetDate };

export const players = [
  {
    id: "890312-007",
    firstName: "Daniel",
    lastName: "Asplund",
    hcp: 0.3,
  },
  {
    id: "880727-002",
    firstName: "Martin",
    lastName: "Ohlsson",
    hcp: 2.5,
  },
  {
    id: "860406-007",
    firstName: "Peter",
    lastName: "Wintzell",
    hcp: 6.7,
  },
  {
    id: "860426-034",
    firstName: "Mikael",
    lastName: "Bolmstam",
    hcp: 9.2,
  },
  {
    id: "870608-012",
    firstName: "Henrik",
    lastName: "Åberg",
    hcp: 9.5,
  },
  {
    id: "850413-003",
    firstName: "Mattias",
    lastName: "Swenson",
    hcp: 9.5,
  },
  {
    id: "881025-025",
    firstName: "Philipp",
    lastName: "Bisping",
    hcp: 11.3,
  },
  {
    id: "871022-011",
    firstName: "Carl",
    lastName: "Wålinder",
    hcp: 11.7,
  },
  {
    id: "750605-006",
    firstName: "Johan",
    lastName: "Alenius",
    hcp: 11.8,
  },
  {
    id: "870416-045",
    firstName: "Tomas",
    lastName: "Johansson",
    hcp: 12.6,
  },
  {
    id: "921224-011",
    firstName: "Rasmus",
    lastName: "Rune",
    hcp: 13.0,
  },
  {
    id: "830209-016",
    firstName: "Kristoffer",
    lastName: "Sporrong",
    hcp: 14.1,
  },
];

/*
{
  id: "860701-026",
  firstName: "Sebastian",
  lastName: "Sporrong",
  hcp: 3.7,
}, */
/*   {
  id: "900323-010",
  firstName: "David",
  lastName: "Brodin",
  hcp: 5.1,
},
{
  id: "790805-021",
  firstName: "Johan",
  lastName: "Emet",
  hcp: 12.2,
},
*/
