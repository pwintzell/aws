import { players } from "./setup-data";

export function findMostEvenTeams() {
  const teamEur = [
    "890312-007",
    "880727-002",
    "860426-034",
    "850413-003",
    "830209-016",
  ];

  const teamUsa = ["871022-011", "860406-007", "870608-012"];

  const newcomers = ["881025-025", "750605-006", "870416-045", "921224-011"];

  const playersDictionary = {};
  players.forEach((player) => (playersDictionary[player.id] = player));

  let bestSoFar = Infinity;
  let bestEurTeam;
  let bestUsaTeam;
  let bestTeamEurTotHcp;
  let bestTeamUsaTotHcp;
  for (let i = 0; i < newcomers.length; i++) {
    const eurCandidate = newcomers[i];
    const usaCandidates = newcomers.slice(0, i).concat(newcomers.slice(i + 1));
    const testTeamEur = [...teamEur, eurCandidate];
    const testTeamUsa = [...teamUsa, ...usaCandidates];
    console.log("### playersDictionary", playersDictionary);

    const teamEurTotHcp = testTeamEur.reduce(
      (acc, playerId) => acc + playersDictionary[playerId].hcp,
      0
    );
    const teamUsaTotHcp = testTeamUsa.reduce(
      (acc, playerId) => acc + playersDictionary[playerId].hcp,
      0
    );

    const difference = Math.abs(teamEurTotHcp - teamUsaTotHcp);
    console.log("Difference for", eurCandidate, "being in EUR:", difference);
    console.log("EUR total hcp", teamEurTotHcp);
    console.log("USA total hcp", teamUsaTotHcp);
    if (difference < bestSoFar) {
      bestSoFar = difference;
      bestEurTeam = testTeamEur;
      bestUsaTeam = testTeamUsa;
      bestTeamEurTotHcp = teamEurTotHcp;
      bestTeamUsaTotHcp = teamUsaTotHcp;
    }
  }

  return {
    eur: bestEurTeam,
    usa: bestUsaTeam,
    eurTotHcp: bestTeamEurTotHcp,
    usaTotHcp: bestTeamUsaTotHcp,
    hcpDifference: Math.round(bestSoFar * 10) / 10,
  };
}
