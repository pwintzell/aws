export const rounds = [
  {
    name: 'Saturday foursomes',
    courseName: 'Varbergs Östra',
    type: 'foursome'
  },
  {
    name: 'Saturday four-ball',
    courseName: 'Varbergs Västra',
    type: 'four-ball'
  },
  {
    name: 'Sunday singles',
    courseName: 'Vallda',
    type: 'single'
  }
]

export const matches = [
  {
    round: 0,
    type: 'foursome',
    teamOneMembers: [
      '860701-026+871022-011'
    ],
    teamTwoMembers: [
      '890312-007+880727-002'
    ]
  },
  {
    round: 0,
    type: 'foursome',
    teamOneMembers: [
      '860406-007+870608-012'
    ],
    teamTwoMembers: [
      '850413-003+830209-016'
    ]
  },
  {
    round: 1,
    type: 'four-ball',
    teamOneMembers: [
      '870608-012',
      '871022-011'
    ],
    teamTwoMembers: [
      '890312-007',
      '850413-003'
    ]
  },
  {
    round: 1,
    type: 'four-ball',
    teamOneMembers: [
      '860701-026',
      '860406-007'
    ],
    teamTwoMembers: [
      '880727-002',
      '830209-016'
    ]
  },
  {
    round: 2,
    type: 'single',
    teamOneMembers: [
      '860406-007'
    ],
    teamTwoMembers: [
      '890312-007'
    ]
  },
  {
    round: 2,
    type: 'single',
    teamOneMembers: [
      '871022-011'
    ],
    teamTwoMembers: [
      '830209-016'
    ]
  },
  {
    round: 2,
    type: 'single',
    teamOneMembers: [
      '870608-012'
    ],
    teamTwoMembers: [
      '880727-002'
    ]
  },
  {
    round: 2,
    type: 'single',
    teamOneMembers: [
      '860701-026'
    ],
    teamTwoMembers: [
      '850413-003'
    ]
  }
]

export const players = [
  {
    id: '860701-026',
    firstName: 'Sebastian',
    lastName: 'Sporrong',
    hcp: 3.4
  },
  {
    id: '890312-007',
    firstName: 'Daniel',
    lastName: 'Asplund',
    hcp: 4.5
  },
  {
    id: '880727-002',
    firstName: 'Martin',
    lastName: 'Ohlsson',
    hcp: 6.8
  },
  {
    id: '860406-007',
    firstName: 'Peter',
    lastName: 'Wintzell',
    hcp: 9.8
  },
  {
    id: '870608-012',
    firstName: 'Henrik',
    lastName: 'Åberg',
    hcp: 11.4
  },
  {
    id: '850413-003',
    firstName: 'Mattias',
    lastName: 'Svensson',
    hcp: 12.5
  },
  {
    id: '871022-011',
    firstName: 'Carl',
    lastName: 'Wålinder',
    hcp: 13.4
  },
  {
    id: '830209-016',
    firstName: 'Kristoffer',
    lastName: 'Sporrong',
    hcp: 15.3
  }
]

export const courses = {
  'Varbergs Västra': [
    {
      par: 4,
      hcp: 15
    },
    {
      par: 5,
      hcp: 7
    },
    {
      par: 4,
      hcp: 9
    },
    {
      par: 4,
      hcp: 5
    },
    {
      par: 4,
      hcp: 1
    },
    {
      par: 5,
      hcp: 11
    },
    {
      par: 4,
      hcp: 3
    },
    {
      par: 3,
      hcp: 17
    },
    {
      par: 4,
      hcp: 13
    },
    {
      par: 3,
      hcp: 14
    },
    {
      par: 4,
      hcp: 12
    },
    {
      par: 4,
      hcp: 2
    },
    {
      par: 4,
      hcp: 6
    },
    {
      par: 3,
      hcp: 10
    },
    {
      par: 5,
      hcp: 4
    },
    {
      par: 3,
      hcp: 8
    },
    {
      par: 4,
      hcp: 18
    },
    {
      par: 5,
      hcp: 16
    }
  ],
  'Varbergs Östra': [
    {
      par: 5,
      hcp: 11
    },
    {
      par: 4,
      hcp: 7
    },
    {
      par: 3,
      hcp: 13
    },
    {
      par: 5,
      hcp: 15
    },
    {
      par: 4,
      hcp: 5
    },
    {
      par: 4,
      hcp: 1
    },
    {
      par: 3,
      hcp: 17
    },
    {
      par: 4,
      hcp: 3
    },
    {
      par: 4,
      hcp: 9
    },
    {
      par: 4,
      hcp: 10
    },
    {
      par: 4,
      hcp: 2
    },
    {
      par: 3,
      hcp: 18
    },
    {
      par: 5,
      hcp: 12
    },
    {
      par: 3,
      hcp: 4
    },
    {
      par: 4,
      hcp: 16
    },
    {
      par: 3,
      hcp: 8
    },
    {
      par: 5,
      hcp: 6
    },
    {
      par: 4,
      hcp: 14
    }
  ],
  'Vallda': [
    {
      par: 4,
      hcp: 12
    },
    {
      par: 4,
      hcp: 10
    },
    {
      par: 4,
      hcp: 14
    },
    {
      par: 5,
      hcp: 4
    },
    {
      par: 3,
      hcp: 18
    },
    {
      par: 5,
      hcp: 2
    },
    {
      par: 4,
      hcp: 6
    },
    {
      par: 3,
      hcp: 8
    },
    {
      par: 4,
      hcp: 16
    },
    {
      par: 5,
      hcp: 7
    },
    {
      par: 3,
      hcp: 11
    },
    {
      par: 4,
      hcp: 5
    },
    {
      par: 4,
      hcp: 1
    },
    {
      par: 4,
      hcp: 17
    },
    {
      par: 3,
      hcp: 15
    },
    {
      par: 5,
      hcp: 13
    },
    {
      par: 4,
      hcp: 3
    },
    {
      par: 4,
      hcp: 9
    }
  ]
}

export const score = {
  0: {
    '860701-026+871022-011': [],
    '890312-007+880727-002': [],
    '860406-007+870608-012': [],
    '850413-003+830209-016': []
  },
  1: {
    '860701-026': [],
    '860406-007': [],
    '890312-007': [],
    '880727-002': [],
    '870608-012': [],
    '871022-011': [],
    '850413-003': [],
    '830209-016': []
  },
  2: {
    '860701-026': [],
    '860406-007': [],
    '890312-007': [],
    '880727-002': [],
    '870608-012': [],
    '871022-011': [],
    '850413-003': [],
    '830209-016': []
  }
}

var express = require('express')
var http = require('http')
var os = require('os')
var path = require('path')

var app = module.exports = express()

/* rounds = JSON.parse(JSON.stringify(rounds));
var openConnections = [];

var sendScoreUpdate = function(newScores){
  sendInfo(newScores);
}

var sendInfo = function(data){
    openConnections.forEach(function(resp) {
      resp.write('event: message' + '\n');
        resp.write('data: ' + JSON.stringify(data) + '\n\n');
    });
}

var updateAddScoreServerSide = function(data){
  var serverRoundScores = results[data.round].scores;
  for (var i=0; i<data.playerScores.length; i++){
    for (var u=0; u<serverRoundScores.length; u++){
      if (serverRoundScores[u].player === data.playerScores[i].player){
        var newScore = data.playerScores[i].score;
        if (typeof serverRoundScores[u].strokes[data.hole] === undefined) {
          serverRoundScores[u].strokes.push(newScore);
        } else {
          serverRoundScores[u].strokes[data.hole] = newScore;
        }
      }
    }
  }
}

var updateRemoveScoreServerSide = function(data){
  var serverRoundScores = results[data.round].scores;
  for (var i=0; i<data.playersToClear.length; i++){
    for (var u=0; u<serverRoundScores.length; u++){
      if (serverRoundScores[u].player === data.playersToClear[i]){
        if (typeof serverRoundScores[u].strokes[data.hole] !== undefined) {
          serverRoundScores[u].strokes.splice(data.hole, 1);
        }
      }
    }
  }
}

app.get('/ajax/push-update', function(req, res) {

    // set timeout as high as possible
    req.socket.setTimeout(0x7FFFFFFF);

    // send headers for event-stream connection
    // see spec for more information
    res.writeHead(200, {
        'Content-Type': 'text/event-stream',
        'Cache-Control': 'no-cache',
        'Connection': 'keep-alive'
    });
    res.write('\n');

    // push this res object to our global variable
    openConnections.push(res);

    // When the request is closed, e.g. the browser window
    // is closed. We search through the open connections
    // array and remove this connection.
    req.on('close', function() {
        var toRemove;
        for (var j =0 ; j < openConnections.length ; j++) {
            if (openConnections[j] == res) {
                toRemove =j;
                break;
            }
        }
        openConnections.splice(toRemove,1);
    });
});

app.get('/ajax/update', function(request, response){
  response.send('\n')
  var data = JSON.parse(request.query.message);
  if (data.type === 'updateScore'){
    updateAddScoreServerSide(data);
    sendScoreUpdate(data);
  } else if (data.type === 'removeScore'){
    updateRemoveScoreServerSide(data);
    sendScoreUpdate(data);
  }
}); */

app.get('/ajax/get-all', function (request, response) {
  var data = {
    rounds: rounds,
    matches: matches,
    players: players,
    courses: courses,
    score: score
  }
  response.send(data)
})
console.log('Running ryder!')
app.use('/', express.static('build'))
app.listen(500)
