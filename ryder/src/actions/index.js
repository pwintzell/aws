export const goToPreviousRound = () => {
  console.log('Action, GO_TO_PREVIOUS_ROUND: ')
  return {
    type: 'GO_TO_PREVIOUS_ROUND'
  }
}

export const goToNextRound = () => {
  console.log('Action, GO_TO_NEXT_ROUND: ')
  return {
    type: 'GO_TO_NEXT_ROUND'
  }
}

export const toggleMatchupScore = (id) => {
  console.log('Action, TOGGLE_MATCHUP_SCORE: ')
  return {
    type: 'TOGGLE_MATCHUP_SCORE',
    id
  }
}

export const nextMatchupHole = (id) => {
  console.log('Action, NEXT_MATCHUP_HOLE: ')
  return {
    type: 'NEXT_MATCHUP_HOLE',
    id
  }
}

export const previousMatchupHole = (id) => {
  console.log('Action, PREVIOUS_MATCHUP_HOLE: ')
  return {
    type: 'PREVIOUS_MATCHUP_HOLE',
    id
  }
}

export const addStroke = (round, playerId, hole) => {
  console.log('Action, ADD_STROKE: ')
  return {
    type: 'ADD_STROKE',
    round,
    playerId,
    hole
  }
}

export const subtractStroke = (round, playerId, hole) => {
  console.log('Action, SUBTRACT_STROKE: ')
  return {
    type: 'SUBTRACT_STROKE',
    round,
    playerId,
    hole
  }
}

export const fetchScheduleHasErrored = (bool) => {
  console.log('Action, FETCH_SCHEDULE_HAS_ERRORED: ')
  return {
    type: 'FETCH_SCHEDULE_HAS_ERRORED',
    hasErrored: bool
  }
}

export const fetchScheduleIsLoading = (bool) => {
  console.log('Action, FETCH_SCHEDULE_IS_LOADING: ')
  return {
    type: 'FETCH_SCHEDULE_IS_LOADING',
    isLoading: bool
  }
}

export const fetchScheduleSuccess = (schedule) => {
  console.log('Action, FETCH_SCHEDULE: ')
  return {
    type: 'FETCH_SCHEDULE',
    schedule
  }
}

export const fetchTotalScoreSuccess = (score) => {
  console.log('Action, FETCH_TOTAL_SCORE: ')
  return {
    type: 'FETCH_TOTAL_SCORE',
    score
  }
}

export const saveMatchupHoleScore = (hole, match, displayedScore) => {
  console.log('Action, SAVE_SCORE: ')
  return {
    type: 'SAVE_SCORE',
    hole,
    match,
    displayedScore
  }
}

export const clearMatchupHoleScore = (hole, match, displayedScore) => {
  console.log('Action, CLEAR_SCORE: ')
  return {
    type: 'CLEAR_SCORE',
    hole,
    match,
    displayedScore
  }
}

/**
 * Initial get schedule
 */
export const fetchSchedule = () => {
  console.log('fetchSchedule action')
  return (dispatch) => {
    dispatch(fetchScheduleIsLoading(true))

    fetch('/ajax/get-all')
      .then(response => response.json())
      .then(schedule => {
        dispatch(fetchScheduleSuccess(schedule))
      })
  }
}

export const fetchTotalScore = () => {
  console.log('fetchTotalScore action')
  return (dispatch) => {
    fetch('/ajax/get-total-score')
      .then(response => response.json())
      .then(score => {
        dispatch(fetchTotalScoreSuccess(score))
      })
  }
}
