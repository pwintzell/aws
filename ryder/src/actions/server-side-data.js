export const rounds = [
  {
    name: 'Saturday foursomes',
    courseName: 'Varbergs Östra',
    type: 'foursome'
  },
  {
    name: 'Saturday four-ball',
    courseName: 'Varbergs Västra',
    type: 'four-ball'
  },
  {
    name: 'Sunday singles',
    courseName: 'Vallda',
    type: 'single'
  }
]

export const matches = [
  {
    round: 0,
    type: 'foursome',
    teamOneMembers: [
      '860701-026+871022-011'
    ],
    teamTwoMembers: [
      '890312-007+880727-002'
    ]
  },
  {
    round: 0,
    type: 'foursome',
    teamOneMembers: [
      '860406-007+870608-012'
    ],
    teamTwoMembers: [
      '850413-003+830209-016'
    ]
  },
  {
    round: 1,
    type: 'four-ball',
    teamOneMembers: [
      '870608-012',
      '871022-011'
    ],
    teamTwoMembers: [
      '890312-007',
      '850413-003'
    ]
  },
  {
    round: 1,
    type: 'four-ball',
    teamOneMembers: [
      '860701-026',
      '860406-007'
    ],
    teamTwoMembers: [
      '880727-002',
      '830209-016'
    ]
  },
  {
    round: 2,
    type: 'single',
    teamOneMembers: [
      '860406-007'
    ],
    teamTwoMembers: [
      '890312-007'
    ]
  },
  {
    round: 2,
    type: 'single',
    teamOneMembers: [
      '871022-011'
    ],
    teamTwoMembers: [
      '830209-016'
    ]
  },
  {
    round: 2,
    type: 'single',
    teamOneMembers: [
      '870608-012'
    ],
    teamTwoMembers: [
      '880727-002'
    ]
  },
  {
    round: 2,
    type: 'single',
    teamOneMembers: [
      '860701-026'
    ],
    teamTwoMembers: [
      '850413-003'
    ]
  }
]

export const players = [
  {
    id: '860701-026',
    firstName: 'Sebastian',
    lastName: 'Sporrong',
    hcp: 3.4
  },
  {
    id: '890312-007',
    firstName: 'Daniel',
    lastName: 'Asplund',
    hcp: 4.5
  },
  {
    id: '880727-002',
    firstName: 'Martin',
    lastName: 'Ohlsson',
    hcp: 6.8
  },
  {
    id: '860406-007',
    firstName: 'Peter',
    lastName: 'Wintzell',
    hcp: 9.8
  },
  {
    id: '870608-012',
    firstName: 'Henrik',
    lastName: 'Åberg',
    hcp: 11.4
  },
  {
    id: '850413-003',
    firstName: 'Mattias',
    lastName: 'Svensson',
    hcp: 12.5
  },
  {
    id: '871022-011',
    firstName: 'Carl',
    lastName: 'Wålinder',
    hcp: 13.4
  },
  {
    id: '830209-016',
    firstName: 'Kristoffer',
    lastName: 'Sporrong',
    hcp: 15.3
  }
]

export const courses = {
  'Varbergs Västra': [
    {
      par: 4,
      hcp: 15
    },
    {
      par: 5,
      hcp: 7
    },
    {
      par: 4,
      hcp: 9
    },
    {
      par: 4,
      hcp: 5
    },
    {
      par: 4,
      hcp: 1
    },
    {
      par: 5,
      hcp: 11
    },
    {
      par: 4,
      hcp: 3
    },
    {
      par: 3,
      hcp: 17
    },
    {
      par: 4,
      hcp: 13
    },
    {
      par: 3,
      hcp: 14
    },
    {
      par: 4,
      hcp: 12
    },
    {
      par: 4,
      hcp: 2
    },
    {
      par: 4,
      hcp: 6
    },
    {
      par: 3,
      hcp: 10
    },
    {
      par: 5,
      hcp: 4
    },
    {
      par: 3,
      hcp: 8
    },
    {
      par: 4,
      hcp: 18
    },
    {
      par: 5,
      hcp: 16
    }
  ],
  'Varbergs Östra': [
    {
      par: 5,
      hcp: 11
    },
    {
      par: 4,
      hcp: 7
    },
    {
      par: 3,
      hcp: 13
    },
    {
      par: 5,
      hcp: 15
    },
    {
      par: 4,
      hcp: 5
    },
    {
      par: 4,
      hcp: 1
    },
    {
      par: 3,
      hcp: 17
    },
    {
      par: 4,
      hcp: 3
    },
    {
      par: 4,
      hcp: 9
    },
    {
      par: 4,
      hcp: 10
    },
    {
      par: 4,
      hcp: 2
    },
    {
      par: 3,
      hcp: 18
    },
    {
      par: 5,
      hcp: 12
    },
    {
      par: 3,
      hcp: 4
    },
    {
      par: 4,
      hcp: 16
    },
    {
      par: 3,
      hcp: 8
    },
    {
      par: 5,
      hcp: 6
    },
    {
      par: 4,
      hcp: 14
    }
  ],
  'Vallda': [
    {
      par: 4,
      hcp: 12
    },
    {
      par: 4,
      hcp: 10
    },
    {
      par: 4,
      hcp: 14
    },
    {
      par: 5,
      hcp: 4
    },
    {
      par: 3,
      hcp: 18
    },
    {
      par: 5,
      hcp: 2
    },
    {
      par: 4,
      hcp: 6
    },
    {
      par: 3,
      hcp: 8
    },
    {
      par: 4,
      hcp: 16
    },
    {
      par: 5,
      hcp: 7
    },
    {
      par: 3,
      hcp: 11
    },
    {
      par: 4,
      hcp: 5
    },
    {
      par: 4,
      hcp: 1
    },
    {
      par: 4,
      hcp: 17
    },
    {
      par: 3,
      hcp: 15
    },
    {
      par: 5,
      hcp: 13
    },
    {
      par: 4,
      hcp: 3
    },
    {
      par: 4,
      hcp: 9
    }
  ]
}

/* export const score = {
  0: {
    '860701-026+871022-011': [
      5,
      4,
      5,
      5,
      5,
      6,
      6,
      3,
      8,
      6,
      5,
      5,
      5,
      3,
      8,
      4,
      5,
      6
    ],
    '890312-007+880727-002': [
      5,
      6,
      6,
      5,
      6,
      6,
      5,
      3,
      4,
      3,
      6,
      4,
      4,
      3,
      9,
      5,
      4,
      7
    ],
    '860406-007+870608-012': [
      5,
      6,
      4,
      5,
      6,
      7,
      4,
      2,
      4,
      3,
      5,
      4,
      3,
      4,
      8,
      3,
      6,
      5
    ],
    '850413-003+830209-016': [
      6,
      8,
      9,
      7,
      5,
      6,
      6,
      4,
      6,
      4,
      6,
      5,
      5,
      3,
      8,
      4,
      5,
      7
    ]
  },
  1: {
    '860701-026': [
      5,
      5,
      4,
      5,
      5,
      6,
      3,
      6,
      4,
      9,
      5,
      3,
      5,
      4,
      4,
      3,
      4,
      4
    ],
    '860406-007': [
      6,
      4,
      3,
      6,
      5,
      5,
      4,
      7,
      4,
      4,
      7,
      3,
      6,
      3,
      5,
      5,
      9,
      4
    ],
    '890312-007': [
      5,
      4,
      4,
      5,
      4,
      5,
      4,
      5,
      5,
      4,
      5,
      4,
      5,
      4,
      5,
      3,
      4,
      5
    ],
    '880727-002': [
      5,
      3,
      4,
      5,
      5,
      7,
      3,
      6,
      6,
      4,
      6,
      3,
      5,
      4,
      7,
      4,
      5,
      4
    ],
    '870608-012': [
      6,
      4,
      3,
      7,
      6,
      6,
      4,
      4,
      6,
      9,
      6,
      4,
      6,
      3,
      6,
      4,
      6,
      9
    ],
    '871022-011': [
      6,
      5,
      4,
      5,
      5,
      6,
      3,
      5,
      4,
      4,
      8,
      3,
      8,
      2,
      9,
      3,
      6,
      4
    ],
    '850413-003': [
      7,
      6,
      5,
      5,
      5,
      7,
      5,
      5,
      5,
      5,
      6,
      4,
      8,
      4,
      5,
      4,
      6,
      9
    ],
    '830209-016': [
      7,
      4,
      4,
      4,
      4,
      8,
      5,
      6,
      5,
      5,
      6,
      3,
      9,
      3,
      5,
      5,
      7,
      4
    ]
  },
  2: {
    '860701-026': [
      5,
      5,
      4,
      4,
      3,
      4,
      5,
      3,
      4,
      5,
      5,
      6,
      5,
      4,
      4,
      3,
      5
    ],
    '860406-007': [
      4,
      4,
      5,
      4,
      2,
      5,
      5,
      3,
      6,
      8,
      5,
      7,
      4,
      5,
      5,
      4,
      5,
      4
    ],
    '890312-007': [
      5,
      4,
      5,
      4,
      3,
      4,
      4,
      3,
      4,
      4,
      5,
      5,
      5,
      5,
      5,
      3,
      5,
      4
    ],
    '880727-002': [
      7,
      6,
      4,
      5,
      4,
      5,
      5,
      4,
      5,
      5,
      6,
      6,
      4,
      5,
      5,
      3,
      6,
      4
    ],
    '870608-012': [
      5,
      7,
      5,
      7,
      4,
      5,
      6,
      2,
      5,
      4,
      6,
      7,
      5,
      6,
      6,
      4,
      6,
      3
    ],
    '871022-011': [
      6,
      4,
      6,
      5,
      3,
      5,
      6,
      4,
      6,
      4,
      5,
      6,
      5,
      6,
      4,
      3,
      9,
      6
    ],
    '850413-003': [
      6,
      6,
      4,
      5,
      4,
      5,
      5,
      4,
      5,
      5,
      6,
      6,
      4,
      5,
      5,
      3,
      6
    ],
    '830209-016': [
      5,
      6,
      6,
      4,
      5,
      5,
      5,
      4,
      5,
      5,
      6,
      8,
      5,
      5,
      5,
      4,
      6,
      5
    ]
  }
} */

export const score = {
  0: {
    '860701-026+871022-011': [],
    '890312-007+880727-002': [],
    '860406-007+870608-012': [],
    '850413-003+830209-016': []
  },
  1: {
    '860701-026': [],
    '860406-007': [],
    '890312-007': [],
    '880727-002': [],
    '870608-012': [],
    '871022-011': [],
    '850413-003': [],
    '830209-016': []
  },
  2: {
    '860701-026': [],
    '860406-007': [],
    '890312-007': [],
    '880727-002': [],
    '870608-012': [],
    '871022-011': [],
    '850413-003': [],
    '830209-016': []
  }
}
