import React from 'react'
import classNames from 'classnames'
import './HolesInfo.css'

const MatchupScore = ({selectedHole, holes, id, matchResults}) => {
  let currentHoleString = 'Hole ' + (selectedHole + 1) + ' - HCP ' + holes[selectedHole].hcp + ' - Par ' + holes[selectedHole].par
  return (
    <div>
      <p className='scoring_info'>
        { currentHoleString }
      </p>
      <div className='scoring_holes'>
        {
          holes.map((hole, index) => {
            let holeClassNames = classNames({
              'scoring_hole': true,
              'scoring_hole--selected': index === selectedHole,
              'scoring_hole--red': matchResults[index] === 'USA',
              'scoring_hole--blue': matchResults[index] === 'EUR',
              'scoring_hole--even': matchResults[index] === 'EVEN',
              'scoring_hole--dead': matchResults[index] === 'DEAD'
            })
            return <div className={holeClassNames} key={id + '' + index} />
          })
        }
      </div>
    </div>
  )
}

export default MatchupScore

/*
      <div className="scoring_holes">
        <div data-ng-repeat="hole in holes" className="scoring_hole"
              data-ng-className="{'scoring_hole--selected': selectedHole === $index, 'scoring_hole--red': getHoleStatus(match, $index) === 'USA', 'scoring_hole--blue': getHoleStatus(match, $index) === 'EUR', 'scoring_hole--even': getHoleStatus(match, $index) === 'EVEN', 'scoring_hole--dead': getHoleStatus(match, $index) === 'DEAD'}"></div>
      </div>
*/
