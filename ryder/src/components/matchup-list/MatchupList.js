import React from 'react'
import Matchup from '../matchup/Matchup'
import MatchupScore from '../matchup-score/MatchupScore'

const MatchupList = ({
  matches,
  players,
  holes,
  selectedRound,
  matchViews,
  score,
  matchesResults,
  displayedScore,
  toggleMatchupScore,
  nextMatchupHole,
  previousMatchupHole,
  addStroke,
  subtractStroke
}) => (
  <div className='matchup-list'>
    {
      matches.filter(match => match.round === selectedRound).map((match, id) =>
        <div key={id}>
          <Matchup
            match={match}
            players={players}
            toggleMatchupScore={() => toggleMatchupScore(id)}
            matchResults={matchesResults[match.round][id]}
          />
          <MatchupScore
            expanded={matchViews[match.round][id]['expanded']}
            selectedHole={matchViews[match.round][id]['selectedHole']}
            holes={holes}
            players={players}
            match={match}
            index={id}
            currentRoundScores={score[match.round]}
            displayedScore={displayedScore}
            matchResults={matchesResults[match.round][id]}
            nextMatchupHole={() => nextMatchupHole(id)}
            previousMatchupHole={() => previousMatchupHole(id)}
          />
        </div>
      )
    }
  </div>
)

export default MatchupList
