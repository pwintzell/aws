import React from 'react'
import { Swipeable } from 'react-touch'
import './MatchupScore.css'
import StrokeHandlingContainer from '../../containers/StrokeHandling'
import Storing from '../../containers/Storing'
import HolesInfo from '../holes-info/HolesInfo'
import * as playerUtils from '../../utils/players'
import * as scoringUtils from '../../utils/scoring'

const MatchupScore = ({expanded, selectedHole, match, players, holes, id, score, displayedScore, matchResults, nextMatchupHole, previousMatchupHole, clearMatchupHoleScore, saveMatchupHoleScore}) => {
  if (!expanded) {
    return null
  }
  return (
    <Swipeable onSwipeRight={previousMatchupHole} onSwipeLeft={nextMatchupHole}>
      <section className='scoring'>
        <HolesInfo
          selectedHole={selectedHole}
          holes={holes}
          id={id}
          matchResults={matchResults}
        />

        <div className='scoring_action-area'>
          <label className='scoring_button-label'>
            <button className='scoring_button' onClick={previousMatchupHole}>&lang;</button>
          </label>

          <div className='scoring_actions'>
            <div className='scoring_action-area'>
              {
                match.teamOneMembers.map((member) =>
                  <StrokeHandlingContainer
                    key={member}
                    initials={playerUtils.getInitials(member, players)}
                    score={scoringUtils.getHoleScore(member, selectedHole, match.round, displayedScore)}
                    round={match.round}
                    hole={selectedHole}
                    playerId={member}
                    color='red'
                  />
                )
              }
              {
                match.teamTwoMembers.map((member) =>
                  <StrokeHandlingContainer
                    key={member}
                    initials={playerUtils.getInitials(member, players)}
                    score={scoringUtils.getHoleScore(member, selectedHole, match.round, displayedScore)}
                    round={match.round}
                    hole={selectedHole}
                    playerId={member}
                    color='blue'
                  />
                )
              }
            </div>
          </div>

          <label className='scoring_button-label scoring_button-label--right'>
            <button className='scoring_button' onClick={nextMatchupHole}>&rang;</button>
          </label>
        </div>

        <Storing
          selectedHole={selectedHole}
          match={match}
          displayedScore={displayedScore}
        />
      </section>
    </Swipeable>
  )
}

export default MatchupScore

/*
    <section className="scoring" data-ng-repeat-end data-ng-show="expanded && match.round === state.round" data-ng-init="holes = getCourse(match.round).holes; selectedHole = 0">
      <p className="scoring_info">
        Hole {{selectedHole+1}} - HCP {{holes[selectedHole].hcp}} - Par {{holes[selectedHole].par}}
      </p>
      <div className="scoring_holes">
        <div data-ng-repeat="hole in holes" className="scoring_hole"
              data-ng-className="{'scoring_hole--selected': selectedHole === $index, 'scoring_hole--red': getHoleStatus(match, $index) === 'USA', 'scoring_hole--blue': getHoleStatus(match, $index) === 'EUR', 'scoring_hole--even': getHoleStatus(match, $index) === 'EVEN', 'scoring_hole--dead': getHoleStatus(match, $index) === 'DEAD'}"></div>
      </div>

      <div className="scoring_action-area">
        <label className="scoring_button-label">
          <button className="scoring_button" data-ng-click="previousHole()">&lang;</button>
        </label>

        <div className="scoring_actions">
          <div className="scoring_action-area">
            <div data-ng-repeat="member in match.teamOneMembers" data-ng-init="member=getPlayerOrCombinedPlayers(member); edited=false;" className="scoring_stroke-handling scoring_stroke-handling--red">
              <span className="scoring_initials">{{getInitials(member)}}</span>
              <button className="scoring_change-strokes" data-ng-click="addStroke()">+</button>
              <span className="scoring_strokes">{{getResultOrDisplayedScore()}}</span>
              <button className="scoring_change-strokes" data-ng-click="removeStroke()">-</button>
            </div>

            <div data-ng-repeat="member in match.teamTwoMembers" data-ng-init="member=getPlayerOrCombinedPlayers(member); edited=false;" className="scoring_stroke-handling scoring_stroke-handling--blue">
              <span className="scoring_initials">{{getInitials(member)}}</span>
              <button className="scoring_change-strokes" data-ng-click="addStroke()">+</button>
              <span className="scoring_strokes">{{getResultOrDisplayedScore()}}</span>
              <button className="scoring_change-strokes" data-ng-click="removeStroke()">-</button>
            </div>
          </div>
        </div>

        <label className="scoring_button-label scoring_button-label--right">
          <button className="scoring_button" data-ng-click="nextHole()">&rang;</button>
        </label>
      </div>

      <div className="scoring_storing--down">
        <button className="scoring_storage-button" data-ng-click="clearScore()">CLEAR</button>
        <button className="scoring_storage-button" data-ng-click="saveScore()">SAVE</button>
      </div>
    </section>
*/
