import React from 'react'
import PlayerNames from './PlayerNames'
import classNames from 'classnames'
import './Matchup.css'

const Matchup = ({match, players, matchResults, toggleMatchupScore}) => {
  let totalScore = matchResults.filter(hole => hole === 'USA').length - matchResults.filter(hole => hole === 'EUR').length
  let sectionClassNames = classNames({
    'matchup': true,
    'matchup--left-leader': totalScore > 0,
    'matchup--right-leader': totalScore < 0
  })
  let scoreString = 'EVEN'
  if (totalScore !== 0) {
    scoreString = Math.abs(totalScore) + ' UP'
  }
  return (
    <section className={sectionClassNames} onClick={toggleMatchupScore}>
      <div className='matchup_name matchup_name--left'>
        <p>
          <PlayerNames
            teamMemberList={match.teamOneMembers}
            players={players}
          />
        </p>
      </div>
      <div className='matchup_score'>
        <p>{scoreString}</p>
      </div>
      <div className='matchup_name matchup_name--right'>
        <p>
          <PlayerNames
            teamMemberList={match.teamTwoMembers}
            players={players}
          />
        </p>
      </div>
    </section>
  )
}

export default Matchup

/*
  <section className="matchup" data-ng-repeat-start="match in matches" data-ng-click="expanded = !expanded" data-ng-show="match.round === state.round" data-ng-className="{'matchup--left-leader': calcMatchResult(match) > 0, 'matchup--right-leader': calcMatchResult(match) < 0}">
    <div className="matchup_name matchup_name--left">
      <p>
        <span data-ng-repeat="member in getBothPlayersIfFoursome(match.teamOneMembers)" data-ng-init='member=getPlayer(member)'>
          <span className="matchup_initial">{{::member.firstName.charAt(0)}}.</span> {{::member.lastName}}
          <span data-ng-if="!$last"><br /></span>
        </span>
      </p>
    </div>
    <div className="matchup_score">
      <p>{{getMatchResultText(calcMatchResult(match))}}</p>
    </div>
    <div className="matchup_name matchup_name--right">
      <p>
        <span data-ng-repeat="member in getBothPlayersIfFoursome(match.teamTwoMembers)" data-ng-init='member=getPlayer(member)'>
          <span className="matchup_initial">{{::member.firstName.charAt(0)}}.</span> {{::member.lastName}}
          <span data-ng-if="!$last"><br /></span>
        </span>
      </p>
    </div>
  </section>
*/
