import React from 'react'
import * as playerUtil from '../../utils/players'
import './Matchup.css'

const PlayerNames = ({teamMemberList, players}) => {
  let teamPlayers = playerUtil.getBothPlayersIfFoursome(teamMemberList).map(member => playerUtil.getPlayer(member, players))
  return (
    <span>
      {
        teamPlayers.map(player =>
          <span key={player.id}>
            <span className='matchup_initial'>{player.firstName.charAt(0)}.</span> {player.lastName}
            <br />
          </span>
        )
      }
    </span>
  )
}

export default PlayerNames
