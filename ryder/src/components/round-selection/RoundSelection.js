import React from 'react'
import { Swipeable } from 'react-touch'
import './RoundSelection.css'

const RoundSelection = ({rounds, selectedRound, onClickPreviousRound, onClickNextRound}) => (
  <Swipeable onSwipeRight={onClickPreviousRound} onSwipeLeft={onClickNextRound}>
    <nav className='round-selection'>
      <button onClick={onClickPreviousRound} className='round-selection_button'>&lang;</button>
      <span className='round-selection_title'>{rounds[selectedRound].name}</span>
      <button onClick={onClickNextRound} className='round-selection_button round-selection_button--right'>&rang;</button>
    </nav>
  </Swipeable>
)

export default RoundSelection
