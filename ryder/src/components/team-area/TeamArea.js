import React from 'react'
import './TeamArea.css'

const TeamArea = ({rounds, matchesResults}) => {
  let usaScore = 0
  let eurScore = 0
  for (let roundNumber = 0; roundNumber < rounds.length; roundNumber++) {
    for (let match of matchesResults[roundNumber]) {
      let totalScore = match.filter(hole => hole === 'USA').length - match.filter(hole => hole === 'EUR').length
      if (totalScore > 0) {
        usaScore++
      } else if (totalScore < 0) {
        eurScore++
      } else {
        usaScore += 0.5
        eurScore += 0.5
      }
    }
  }
  return (
    <header>
      <div className='team-area'>
        <p className='team-area_paragrapgh'>
          <span className='team-area_name'>USA</span><br />
          <img className='team-area_flag' src='img/usa.png' alt='USA' />
        </p>
        <span className='team-area_score team-area_score--red'>{Math.floor(usaScore)}</span>
        <span className='team-area_fractal team-area_fractal--red'>{String(usaScore - Math.floor(usaScore)).replace('0', '').replace('.', ',')}</span>
      </div>
      <div className='team-area team-area--right'>
        <span className='team-area_score team-area_score--blue'>{Math.floor(eurScore)}</span>
        <span className='team-area_fractal team-area_fractal--blue'>{String(eurScore - Math.floor(eurScore)).replace('0', '').replace('.', ',')}</span>
        <p className='team-area_paragrapgh team-area_paragrapgh--right'>
          <span className='team-area_name'>EUR</span><br />
          <img className='team-area_flag' src='img/eur.png' alt='EUR' />
        </p>
      </div>
    </header>
  )
}

export default TeamArea
