import './App.css'
import React from 'react'
import Loading from '../components/loading/Loading'
import TeamAreaContainer from './TeamArea'
import RoundSelectionContainer from './RoundSelection'
import MatchupList from './MatchupList'
import { connect } from 'react-redux'
import { fetchSchedule } from '../actions'

class App extends React.Component {
  componentDidMount () {
    const { dispatch } = this.props
    dispatch(fetchSchedule())
  }

  render () {
    const { display } = this.props
    if (display === 'LOADING') {
      return (
        <div>
          <Loading />
        </div>
      )
    } else {
      return (
        <div className='app'>
          <TeamAreaContainer />
          <RoundSelectionContainer />
          <MatchupList />
        </div>
      )
    }
  }
}

const mapStateToProps = (state, ownProps) => ({
  display: state.view.display
})

export default connect(mapStateToProps)(App)
