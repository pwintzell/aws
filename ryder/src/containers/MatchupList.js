import { connect } from 'react-redux'
import { toggleMatchupScore, nextMatchupHole, previousMatchupHole } from '../actions'
import MatchupList from '../components/matchup-list/MatchupList'

const mapStateToProps = (state, ownProps) => ({
  matches: state.scheduling.matches,
  players: state.scheduling.players,
  selectedRound: state.view.selectedRound,
  holes: state.scheduling.courses[state.scheduling.rounds[state.view.selectedRound].courseName],
  score: state.scoring.score,
  displayedScore: state.view.displayedScore,
  matchViews: state.view.matchViews,
  matchesResults: state.scoring.matchesResults
})

const mapDispatchToProps = (dispatch, ownProps) => ({
  toggleMatchupScore: (id) => {
    console.log('toggleMatchupScore')
    dispatch(toggleMatchupScore(id))
  },
  nextMatchupHole: (id) => {
    console.log('nextMatchupHole')
    dispatch(nextMatchupHole(id))
  },
  previousMatchupHole: (id) => {
    console.log('previousMatchupHole')
    dispatch(previousMatchupHole(id))
  }
})

const MatchupListContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(MatchupList)

export default MatchupListContainer
