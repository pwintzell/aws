import { connect } from 'react-redux'
import { goToPreviousRound, goToNextRound } from '../actions'
import RoundSelection from '../components/round-selection/RoundSelection'

const mapStateToProps = (state, ownProps) => ({
	rounds: state.scheduling.rounds,
	selectedRound: state.view.selectedRound
})

const mapDispatchToProps = (dispatch, ownProps) => ({
	onClickPreviousRound: () => {
		console.log('onClickPreviousRound');
		dispatch(goToPreviousRound());
	},
	onClickNextRound: () => {
		console.log('onClickNextRound');
		dispatch(goToNextRound());
	}
})

const RoundSelectionContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(RoundSelection)

export default RoundSelectionContainer