import { connect } from 'react-redux'
import { saveMatchupHoleScore, clearMatchupHoleScore } from '../actions'
import Storing from '../components/storing/Storing'

const mapStateToProps = (state, ownProps) => ({
  rounds: state.scheduling.rounds,
  selectedRound: state.view.selectedRound
})

const mapDispatchToProps = (dispatch, ownProps) => ({
  saveMatchupHoleScore: () => {
    console.log('saveMatchupHoleScore')
    dispatch(saveMatchupHoleScore(ownProps.selectedHole, ownProps.match, ownProps.displayedScore))
  },
  clearMatchupHoleScore: () => {
    console.log('clearMatchupHoleScore')
    dispatch(clearMatchupHoleScore(ownProps.selectedHole, ownProps.match, ownProps.displayedScore))
  }
})

const StoringContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Storing)

export default StoringContainer
