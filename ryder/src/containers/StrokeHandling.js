import { connect } from 'react-redux'
import { addStroke, subtractStroke } from '../actions'
import StrokeHandling from '../components/stroke-handling/StrokeHandling'

const mapStateToProps = (state, ownProps) => ({
  /* matches: state.scheduling.matches,
  players: state.scheduling.players,
  selectedRound: state.view.selectedRound,
  holes: state.scheduling.courses[state.scheduling.rounds[state.view.selectedRound].courseName],
  score: state.scoring.score,
  displayedScore: state.scoring.displayedScore,
  matchViews: state.view.matchViews */
})

const mapDispatchToProps = (dispatch, ownProps) => ({
  addStroke: () => {
    console.log('addStroke')
    dispatch(addStroke(ownProps.round, ownProps.playerId, ownProps.hole))
  },
  subtractStroke: () => {
    console.log('subtractStroke')
    dispatch(subtractStroke(ownProps.round, ownProps.playerId, ownProps.hole))
  }
})

const StrokeHandlingContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(StrokeHandling)

export default StrokeHandlingContainer
