import { connect } from 'react-redux'
import TeamArea from '../components/team-area/TeamArea'

const mapStateToProps = (state, ownProps) => ({
  rounds: state.scheduling.rounds,
  matchesResults: state.scoring.matchesResults
})

const TeamAreaContainer = connect(
  mapStateToProps
)(TeamArea)

export default TeamAreaContainer
