import React from 'react'
import ReactDOM from 'react-dom'
import { createStore, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import thunk from 'redux-thunk'
import { fetchSchedule, fetchTotalScore } from './actions'
import reducer from './reducers'
import App from './containers/App'
import './index.css'

const source = new EventSource('/ajax/push-update')
source.addEventListener('action', function (event) {
  let action = JSON.parse(event.data)
  action['fromServer'] = true
  store.dispatch(action)
})

const sendMessage = function (params) {
  fetch('/ajax/update', {
    method: 'post',
    headers: {
      'Accept': 'application/json, text/plain, */*',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(params)
  }).then(() => console.log('Send update to server success'))
}

const customMiddleWare = store => next => action => {
  next(action)
  if (action.fromServer) {
  	return
  }
  if (action.type === 'SAVE_SCORE' || action.type === 'CLEAR_SCORE') {
  	sendMessage(action)
  }
}

const middleware = [ thunk, customMiddleWare ]
const store = createStore(
  reducer,
  applyMiddleware(...middleware)
)
window.store = store

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
)

window.onfocus = function () {
  store.dispatch(fetchTotalScore())
}
