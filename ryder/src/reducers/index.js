import { combineReducers } from 'redux'
import view from './view'
import scheduling from './scheduling'
import scoring from './scoring'

const ryder = combineReducers({
  scheduling,
  scoring,
  view
})

export default ryder
