/* const initialState = {
  rounds : [
    {
      name: 'Saturday foursomes',
      courseName: 'Varbergs Västra',
      type: 'foursome'
    },
    {
      name: 'Saturday four-ball',
      courseName: 'Varbergs Östra',
      type: 'four-ball'
    },
    {
      name: 'Sunday singles',
      courseName: 'Falkenbergs GK',
      type: 'single'
    }
  ]
} */

const scheduling = (state = {}, action) => {
  console.log('Scheduling, current state: ' + JSON.stringify(state))
  console.log('Scheduling, action: ' + JSON.stringify(action))
  switch (action.type) {
    case 'FETCH_SCHEDULE':
      return {
        rounds: action.schedule.rounds,
        courses: action.schedule.courses,
        matches: action.schedule.matches,
        players: action.schedule.players
      }
    default:
      return state
  }
}

export default scheduling
