import * as scoringUtil from '../utils/scoring'

const storeAndGetNewScoreObject = (score, hole, match, displayedScore) => {
  let newScore = Object.assign({}, score)
  let playerIds = match.teamOneMembers.concat(match.teamTwoMembers)
  if (hole !== 0) {
    for (let playerId of playerIds) {
      if (newScore[match.round][playerId].length < hole) {
        return newScore
      }
    }
  }
  for (let playerId of playerIds) {
    newScore[match.round][playerId][hole] = displayedScore[match.round][playerId][hole]
  }
  return newScore
}

const clearAndGetNewScoreObject = (score, hole, match, displayedScore) => {
  let newScore = Object.assign({}, score)
  let playerIds = match.teamOneMembers.concat(match.teamTwoMembers)
  for (let playerId of playerIds) {
    if (newScore[match.round][playerId].length - 1 !== hole) {
      return newScore
    }
  }
  for (let playerId of playerIds) {
    newScore[match.round][playerId].pop()
  }
  return newScore
}

const scoring = (state = {}, action) => {
  console.log('Scoring, current state: ' + JSON.stringify(state))
  console.log('Scoring, action: ' + JSON.stringify(action))
  let matchesResults
  let score
  switch (action.type) {
    case 'SAVE_SCORE':
      score = storeAndGetNewScoreObject(state.score, action.hole, action.match, action.displayedScore)
      matchesResults = scoringUtil.getMatchesResults(
        state.score,
        state.matches,
        state.rounds,
        state.courses,
        state.players
      )
      return Object.assign({}, state, {
        score: score,
        matchesResults: matchesResults
      })
    case 'CLEAR_SCORE':
      score = clearAndGetNewScoreObject(state.score, action.hole, action.match, action.displayedScore)
      matchesResults = scoringUtil.getMatchesResults(
        state.score,
        state.matches,
        state.rounds,
        state.courses,
        state.players
      )
      return Object.assign({}, state, {
        score: score,
        matchesResults: matchesResults
      })
    case 'FETCH_TOTAL_SCORE':
      console.log('FETCH_TOTAL_SCORE')
      console.log(action)
      matchesResults = scoringUtil.getMatchesResults(
        action.score,
        state.matches,
        state.rounds,
        state.courses,
        state.players
      )
      return Object.assign({}, state, {
        score: action.score,
        matchesResults: matchesResults
      })
    case 'FETCH_SCHEDULE':
      console.log('FETCH_SCHEDULE')
      console.log(action)
      matchesResults = scoringUtil.getMatchesResults(
        action.schedule.score,
        action.schedule.matches,
        action.schedule.rounds,
        action.schedule.courses,
        action.schedule.players
      )
      return {
        score: action.schedule.score,
        matches: action.schedule.matches,
        rounds: action.schedule.rounds,
        courses: action.schedule.courses,
        players: action.schedule.players,
        matchesResults: matchesResults
      }
    default:
      return state
  }
}

export default scoring
