import * as scoringUtil from '../utils/scoring'
const LOADING = 'LOADING'
const STANDARD = 'STANDARD'
// const ERROR = 'ERROR';

const initialState = {
  totalRounds: 3,
  roundMaxes: {},
  selectedRound: 0,
  selectedHole: 0,
  matchViews: {},
  display: LOADING
}

const convertToMatchViews = (rounds, matches) => {
  let matchViews = {}
  for (let roundIndex = 0; roundIndex < rounds.length; ++roundIndex) {
    let thisRoundMatches = matches.filter(match => match.round === roundIndex)
    matchViews[roundIndex] = {}
    for (let matchIndex = 0; matchIndex < thisRoundMatches.length; ++matchIndex) {
      matchViews[roundIndex][matchIndex] = {
        expanded: false,
        selectedHole: 0
      }
    }
  }
  return matchViews
}

const convertToRoundMaxes = (rounds, courses) => {
  let roundMaxes = {}
  for (let roundIndex = 0; roundIndex < rounds.length; ++roundIndex) {
    roundMaxes[roundIndex] = courses[rounds[roundIndex]['courseName']].length
  }
  return roundMaxes
}

const toggleMatch = (matchViews, selectedRound, matchId) => {
  let newMatchViews = Object.assign({}, matchViews)
  newMatchViews[selectedRound][matchId]['expanded'] = !newMatchViews[selectedRound][matchId]['expanded']
  return newMatchViews
}

const nextMatchupHole = (matchViews, selectedRound, roundMaxes, matchId) => {
  let newMatchViews = Object.assign({}, matchViews)
  newMatchViews[selectedRound][matchId]['selectedHole'] = Math.min(roundMaxes[selectedRound] - 1, newMatchViews[selectedRound][matchId]['selectedHole'] + 1)
  return newMatchViews
}

const previousMatchupHole = (matchViews, selectedRound, matchId) => {
  let newMatchViews = Object.assign({}, matchViews)
  newMatchViews[selectedRound][matchId]['selectedHole'] = Math.max(0, newMatchViews[selectedRound][matchId]['selectedHole'] - 1)
  return newMatchViews
}

const addStroke = (displayedScore, round, playerId, hole) => {
  let newDisplayedScore = Object.assign({}, displayedScore)
  newDisplayedScore[round][playerId][hole] += 1
  return newDisplayedScore
}

const subtractStroke = (displayedScore, round, playerId, hole) => {
  let newDisplayedScore = Object.assign({}, displayedScore)
  newDisplayedScore[round][playerId][hole] = Math.max(1, newDisplayedScore[round][playerId][hole] - 1)
  return newDisplayedScore
}

const storeAndGetNewScoreObject = (oldDisplayedScore, hole, match, eventDisplayedScore) => {
  let newScore = Object.assign({}, oldDisplayedScore)
  let playerIds = match.teamOneMembers.concat(match.teamTwoMembers)
  if (hole !== 0) {
    for (let playerId of playerIds) {
      if (newScore[match.round][playerId].length < hole) {
        return newScore
      }
    }
  }
  for (let playerId of playerIds) {
    newScore[match.round][playerId][hole] = eventDisplayedScore[match.round][playerId][hole]
  }
  return newScore
}

const clearAndGetNewScoreObject = (oldDisplayedScore, hole, match, eventDisplayedScore) => {
  let newScore = Object.assign({}, oldDisplayedScore)
  let playerIds = match.teamOneMembers.concat(match.teamTwoMembers)
  for (let playerId of playerIds) {
    if (newScore[match.round][playerId].length - 1 !== hole) {
      return newScore
    }
  }
  for (let playerId of playerIds) {
    newScore[match.round][playerId][hole].pop()
  }
  return newScore
}

const updateDisplayedWithNewTotalScore = (displayedScore, newScore) => {
  let newDisplayedScore = Object.assign({}, displayedScore)
  for (let round of Object.keys(newScore)) {
    for (let player of Object.keys(newScore[round])) {
      for (let holeNumber = 0; holeNumber < newScore[round][player].length; holeNumber++) {
        newDisplayedScore[round][player][holeNumber] = newScore[round][player][holeNumber]
      }
    }
  }
  return newDisplayedScore
}

const view = (state = initialState, action) => {
  console.log('View, current state: ' + JSON.stringify(state))
  console.log('View, action: ' + JSON.stringify(action))
  let selectedRound
  let displayedScore
  switch (action.type) {
    case 'ADD_STROKE':
      return Object.assign({}, state, {
        displayedScore: addStroke(state.displayedScore, action.round, action.playerId, action.hole)
      })
    case 'SUBTRACT_STROKE':
      return Object.assign({}, state, {
        displayedScore: subtractStroke(state.displayedScore, action.round, action.playerId, action.hole)
      })
    case 'TOGGLE_MATCHUP_SCORE':
      return Object.assign({}, state, {
        matchViews: toggleMatch(state.matchViews, state.selectedRound, action.id)
      })
    case 'NEXT_MATCHUP_HOLE':
      return Object.assign({}, state, {
        matchViews: nextMatchupHole(state.matchViews, state.selectedRound, state.roundMaxes, action.id)
      })
    case 'PREVIOUS_MATCHUP_HOLE':
      return Object.assign({}, state, {
        matchViews: previousMatchupHole(state.matchViews, state.selectedRound, action.id)
      })
    case 'GO_TO_PREVIOUS_ROUND':
      selectedRound = Math.max(0, state.selectedRound - 1)
      return Object.assign({}, state, {
        selectedRound: selectedRound
      })
    case 'GO_TO_NEXT_ROUND':
      selectedRound = Math.min(state.totalRounds - 1, state.selectedRound + 1)
      return Object.assign({}, state, {
        selectedRound: selectedRound
      })
    case 'SAVE_SCORE':
      displayedScore = storeAndGetNewScoreObject(state.displayedScore, action.hole, action.match, action.displayedScore)
      return Object.assign({}, state, {
        displayedScore: displayedScore
      })
    case 'CLEAR_SCORE':
      displayedScore = clearAndGetNewScoreObject(state.displayedScore, action.hole, action.match, action.displayedScore)
      return Object.assign({}, state, {
        displayedScore: displayedScore
      })
    case 'FETCH_TOTAL_SCORE':
      displayedScore = updateDisplayedWithNewTotalScore(state.displayedScore, action.score)
      return Object.assign({}, state, {
        displayedScore: displayedScore
      })
    case 'FETCH_SCHEDULE':
      displayedScore = scoringUtil.getSuggestedScores(
        action.schedule.rounds,
        action.schedule.courses,
        action.schedule.matches,
        action.schedule.players
      )
      console.log('Before updateDisplayedWithNewTotalScore')
      console.log(displayedScore)
      displayedScore = updateDisplayedWithNewTotalScore(displayedScore, action.schedule.score)
      console.log('After updateDisplayedWithNewTotalScore')
      console.log(displayedScore)
      return Object.assign({}, state, {
        totalRounds: action.schedule.rounds.length,
        matchViews: convertToMatchViews(action.schedule.rounds, action.schedule.matches),
        roundMaxes: convertToRoundMaxes(action.schedule.rounds, action.schedule.courses),
        displayedScore: displayedScore,
        display: STANDARD
      })
    default:
      return state
  }
}

export default view
