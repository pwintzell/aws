const goToPreviousRound = () => {
  console.log('Action, GO_TO_PREVIOUS_ROUND: ')
  return {
    type: 'GO_TO_PREVIOUS_ROUND'
  }
}

const goToNextRound = () => {
  console.log('Action, GO_TO_NEXT_ROUND: ')
  return {
    type: 'GO_TO_NEXT_ROUND'
  }
}

const toggleMatchupScore = (id) => {
  console.log('Action, TOGGLE_MATCHUP_SCORE: ')
  return {
    type: 'TOGGLE_MATCHUP_SCORE',
    id
  }
}

const nextMatchupHole = (id) => {
  console.log('Action, NEXT_MATCHUP_HOLE: ')
  return {
    type: 'NEXT_MATCHUP_HOLE',
    id
  }
}

const previousMatchupHole = (id) => {
  console.log('Action, PREVIOUS_MATCHUP_HOLE: ')
  return {
    type: 'PREVIOUS_MATCHUP_HOLE',
    id
  }
}

const addStroke = (round, playerId, hole) => {
  console.log('Action, ADD_STROKE: ')
  return {
    type: 'ADD_STROKE',
    round,
    playerId,
    hole
  }
}

const subtractStroke = (round, playerId, hole) => {
  console.log('Action, SUBTRACT_STROKE: ')
  return {
    type: 'SUBTRACT_STROKE',
    round,
    playerId,
    hole
  }
}

const fetchScheduleHasErrored = (bool) => {
  console.log('Action, FETCH_SCHEDULE_HAS_ERRORED: ')
  return {
    type: 'FETCH_SCHEDULE_HAS_ERRORED',
    hasErrored: bool
  }
}

const fetchScheduleIsLoading = (bool) => {
  console.log('Action, FETCH_SCHEDULE_IS_LOADING: ')
  return {
    type: 'FETCH_SCHEDULE_IS_LOADING',
    isLoading: bool
  }
}

const fetchScheduleSuccess = (schedule) => {
  console.log('Action, FETCH_SCHEDULE: ')
  return {
    type: 'FETCH_SCHEDULE',
    schedule
  }
}

const saveMatchupHoleScore = (hole, match, displayedScore) => {
  console.log('Action, SAVE_SCORE: ')
  return {
    type: 'SAVE_SCORE',
    hole,
    match,
    displayedScore
  }
}

const clearMatchupHoleScore = (hole, match, displayedScore) => {
  console.log('Action, CLEAR_SCORE: ')
  return {
    type: 'CLEAR_SCORE',
    hole,
    match,
    displayedScore
  }
}

/**
 * Initial get schedule
 */
const fetchSchedule = () => {
  console.log('fetchSchedule action')
  return (dispatch) => {
    dispatch(fetchScheduleIsLoading(true))

    fetch('/ajax/get-all')
      .then(response => response.json())
      .then(schedule => {
        dispatch(fetchScheduleSuccess(schedule))
      })

    /* setTimeout(() => {
      const schedule = {
        rounds: scheduleData.rounds,
        matches: scheduleData.matches,
        players: scheduleData.players,
        courses: scheduleData.courses,
        score: scheduleData.score
      }
      dispatch(fetchScheduleSuccess(schedule))
    }, 1) */
  }
}
