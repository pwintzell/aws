const express = require('express')
const http = require('http')
const os = require('os')
const path = require('path')
const bodyParser = require('body-parser')
import { createStore } from 'redux'
import reducers from './reducers'
import * as schedule from './setup-data.js'
import { fetchScheduleSuccess } from '../actions'

const store = createStore(reducers)
store.dispatch(fetchScheduleSuccess(schedule))

let app = module.exports = express()
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

let openConnections = []

let sendAction = function (action) {
  console.log('Sending action for one device')
  sendInfo(action)
}

let sendInfo = function (data) {
  openConnections.forEach(function (resp) {
    resp.write('event: action' + '\n')
    resp.write('data: ' + JSON.stringify(data) + '\n\n')
  })
}

app.get('/ajax/push-update', function (req, res) {
  // set timeout as high as possible
  req.socket.setTimeout(0x7FFFFFFF)

  // send headers for event-stream connection
  // see spec for more information
  res.writeHead(200, {
    'Content-Type': 'text/event-stream',
    'Cache-Control': 'no-cache',
    'Connection': 'keep-alive'
  })
  res.write('\n')

  // push this res object to our global letiable
  openConnections.push(res)

  // When the request is closed, e.g. the browser window
  // is closed. We search through the open connections
  // array and remove this connection.
  req.on('close', function () {
    let toRemove
    for (let j = 0; j < openConnections.length; j++) {
      if (openConnections[j] == res) {
        toRemove = j
        break
      }
    }
    openConnections.splice(toRemove, 1)
  })
})

app.post('/ajax/update', function (request, response) {
  response.send('\n')
  let action = request.body
  store.dispatch(action)
  sendAction(action)
})

app.get('/ajax/get-total-score', function (request, response) {
  console.log('Get total score')
  let state = store.getState()
  response.send(state.scoring.score)
})

app.get('/ajax/get-all', function (request, response) {
  let state = store.getState()
  let scheduling = Object.assign({}, state.scheduling)
  scheduling['score'] = state.scoring.score
  response.send(scheduling)
})

console.log('Running ryder!')
app.use('/', express.static('build'))
app.listen(500)
