import { combineReducers } from 'redux'
import scoring from '../reducers/scoring'
import scheduling from '../reducers/scheduling'

const ryder = combineReducers({
  scoring,
  scheduling
})

export default ryder
