export const rounds = [
  {
    name: 'Saturday foursomes',
    courseName: 'Varbergs Östra',
    type: 'foursome'
  },
  {
    name: 'Saturday four-ball',
    courseName: 'Varbergs Västra',
    type: 'four-ball'
  },
  {
    name: 'Sunday singles',
    courseName: 'Falkenberg',
    type: 'single'
  }
]  

export const matches = [
  {
    round: 0,
    type: 'foursome',
    teamOneMembers: [
      '871022-011+860406-007'
    ],
    teamTwoMembers: [
      '890312-007+830209-016'
    ]
  },
  {
    round: 0,
    type: 'foursome',
    teamOneMembers: [
      '750605-006+870608-012'
    ],
    teamTwoMembers: [
      '880727-002+850413-003'
    ]
  },
  {
    round: 0,
    type: 'foursome',
    teamOneMembers: [
      '870416-045+881025-025'
    ],
    teamTwoMembers: [
      '860426-034+921224-011'
    ]
  },
  {
    round: 1,
    type: 'four-ball',
    teamOneMembers: [
      '871022-011',
      '870608-012'
    ],
    teamTwoMembers: [
      '880727-002',
      '921224-011'
    ]
  },
  {
    round: 1,
    type: 'four-ball',
    teamOneMembers: [
      '860406-007',
      '870416-045'
    ],
    teamTwoMembers: [
      '860426-034',
      '830209-016'
    ]
  },
  {
    round: 1,
    type: 'four-ball',
    teamOneMembers: [
      '881025-025',
      '750605-006'
    ],
    teamTwoMembers: [
      '890312-007',
      '850413-003'
    ]
  },
  {
    round: 2,
    type: 'single',
    teamOneMembers: [
      '750605-006'
    ],
    teamTwoMembers: [
      '921224-011'
    ]
  },
  {
    round: 2,
    type: 'single',
    teamOneMembers: [
      '871022-011'
    ],
    teamTwoMembers: [
      '850413-003'
    ]
  },
  {
    round: 2,
    type: 'single',
    teamOneMembers: [
      '860406-007'
    ],
    teamTwoMembers: [
      '830209-016'
    ]
  },
  {
    round: 2,
    type: 'single',
    teamOneMembers: [
      '870416-045'
    ],
    teamTwoMembers: [
      '890312-007'
    ]
  },
  {
    round: 2,
    type: 'single',
    teamOneMembers: [
      '881025-025'
    ],
    teamTwoMembers: [
      '880727-002'
    ]
  },
  {
    round: 2,
    type: 'single',
    teamOneMembers: [
      '870608-012'
    ],
    teamTwoMembers: [
      '860426-034'
    ]
  }
]

export const players = [
  {
    id: "890312-007",
    firstName: "Daniel",
    lastName: "Asplund",
    hcp: 1.2,
  },
  {
    id: "880727-002",
    firstName: "Martin",
    lastName: "Ohlsson",
    hcp: 2.5,
  },
  {
    id: "860406-007",
    firstName: "Peter",
    lastName: "Wintzell",
    hcp: 7.0,
  },
  {
    id: "860426-034",
    firstName: "Mikael",
    lastName: "Bolmstam",
    hcp: 9.1,
  },
  {
    id: "870608-012",
    firstName: "Henrik",
    lastName: "Åberg",
    hcp: 9.5,
  },
  {
    id: "850413-003",
    firstName: "Mattias",
    lastName: "Swenson",
    hcp: 9.8,
  },
  {
    id: "881025-025",
    firstName: "Philipp",
    lastName: "Bisping",
    hcp: 11.3,
  },
  {
    id: "871022-011",
    firstName: "Carl",
    lastName: "Wålinder",
    hcp: 11.6,
  },
  {
    id: "750605-006",
    firstName: "Johan",
    lastName: "Alenius",
    hcp: 12.0,
  },
  {
    id: "870416-045",
    firstName: "Tomas",
    lastName: "Johansson",
    hcp: 12.6,
  },
  {
    id: "921224-011",
    firstName: "Rasmus",
    lastName: "Rune",
    hcp: 13.4,
  },
  {
    id: "830209-016",
    firstName: "Kristoffer",
    lastName: "Sporrong",
    hcp: 14.1,
  },
];

export const courses = {
  'Varbergs Västra': [
    {
      par: 4,
      hcp: 15
    },
    {
      par: 5,
      hcp: 7
    },
    {
      par: 4,
      hcp: 9
    },
    {
      par: 4,
      hcp: 5
    },
    {
      par: 4,
      hcp: 1
    },
    {
      par: 5,
      hcp: 11
    },
    {
      par: 4,
      hcp: 3
    },
    {
      par: 3,
      hcp: 17
    },
    {
      par: 4,
      hcp: 13
    },
    {
      par: 3,
      hcp: 14
    },
    {
      par: 4,
      hcp: 12
    },
    {
      par: 4,
      hcp: 2
    },
    {
      par: 4,
      hcp: 6
    },
    {
      par: 3,
      hcp: 10
    },
    {
      par: 5,
      hcp: 4
    },
    {
      par: 3,
      hcp: 8
    },
    {
      par: 4,
      hcp: 18
    },
    {
      par: 5,
      hcp: 16
    }
  ],
  'Varbergs Östra': [
    {
      par: 5,
      hcp: 11
    },
    {
      par: 4,
      hcp: 7
    },
    {
      par: 3,
      hcp: 13
    },
    {
      par: 5,
      hcp: 15
    },
    {
      par: 4,
      hcp: 5
    },
    {
      par: 4,
      hcp: 1
    },
    {
      par: 3,
      hcp: 17
    },
    {
      par: 4,
      hcp: 3
    },
    {
      par: 4,
      hcp: 9
    },
    {
      par: 4,
      hcp: 10
    },
    {
      par: 4,
      hcp: 2
    },
    {
      par: 3,
      hcp: 18
    },
    {
      par: 5,
      hcp: 12
    },
    {
      par: 3,
      hcp: 4
    },
    {
      par: 4,
      hcp: 16
    },
    {
      par: 3,
      hcp: 8
    },
    {
      par: 5,
      hcp: 6
    },
    {
      par: 4,
      hcp: 14
    }
  ],
  'Falkenberg': [
    {
      par: 5,
      hcp: 17
    },
    {
      par: 4,
      hcp: 1
    },
    {
      par: 4,
      hcp: 3
    },
    {
      par: 4,
      hcp: 5
    },
    {
      par: 3,
      hcp: 13
    },
    {
      par: 4,
      hcp: 7
    },
    {
      par: 5,
      hcp: 11
    },
    {
      par: 3,
      hcp: 15
    },
    {
      par: 4,
      hcp: 9
    },
    {
      par: 4,
      hcp: 12
    },
    {
      par: 4,
      hcp: 2
    },
    {
      par: 5,
      hcp: 16
    },
    {
      par: 4,
      hcp: 8
    },
    {
      par: 4,
      hcp: 4
    },
    {
      par: 4,
      hcp: 6
    },
    {
      par: 3,
      hcp: 10
    },
    {
      par: 5,
      hcp: 18
    },
    {
      par: 3,
      hcp: 14
    }
  ]
}

export const score = {
  0:{
    "871022-011+860406-007":[
       6,
       6,
       4,
       5,
       5,
       5,
       5,
       6,
       5,
       5,
       8,
       3,
       6,
       5,
       5,
       3,
       5,
       4
    ],
    "890312-007+830209-016":[
       6,
       6,
       3,
       5,
       5,
       5,
       4,
       5,
       4,
       6,
       6,
       5,
       5,
       3,
       4,
       3,
       5,
       4
    ],
    "750605-006+870608-012":[
       6,
       4,
       6,
       6,
       6,
       6,
       3,
       6,
       6,
       6,
       6,
       4,
       7,
       4,
       5,
       4,
       7,
       5
    ],
    "880727-002+850413-003":[
       5,
       5,
       6,
       4,
       5,
       6,
       3,
       6,
       4,
       5,
       6,
       3,
       6,
       5,
       6,
       4,
       6,
       4
    ],
    "870416-045+881025-025":[
       6,
       6,
       5,
       4,
       6,
       6,
       3,
       5,
       6,
       5,
       6,
       4,
       6,
       4,
       6,
       5,
       7,
       5
    ],
    "860426-034+921224-011":[
       6,
       5,
       3,
       6,
       6,
       5,
       4,
       4,
       5,
       6,
       5,
       3,
       6,
       3,
       5,
       4,
       7,
       3
    ]
 },
 1:{
    "871022-011":[
       5,
       6,
       5,
       6,
       9,
       6,
       5,
       3,
       6,
       8,
       5,
       5,
       5,
       3,
       6,
       4,
       4
    ],
    "860406-007":[
       5,
       5,
       9,
       4,
       4,
       9,
       6,
       4,
       6,
       3,
       6,
       4,
       4,
       5,
       6,
       5,
       4,
       4
    ],
    "890312-007":[
       9,
       6,
       4,
       4,
       9,
       6,
       9,
       4,
       5,
       5,
       4,
       4,
       5,
       4,
       4,
       5,
       4,
       5
    ],
    "880727-002":[
       5,
       6,
       5,
       5,
       6,
       6,
       5,
       4,
       8,
       4,
       5,
       5,
       4,
       3,
       5,
       4,
       4
    ],
    "870608-012":[
       4,
       10,
       6,
       6,
       7,
       7,
       5,
       3,
       4,
       3,
       9,
       6,
       8,
       7,
       7,
       3,
       7
    ],
    "881025-025":[
       9,
       9,
       6,
       6,
       5,
       5,
       9,
       4,
       5,
       4,
       5,
       9,
       5,
       9,
       6,
       4,
       9,
       6
    ],
    "860426-034":[
       4,
       8,
       6,
       9,
       8,
       5,
       6,
       4,
       4,
       3,
       4,
       9,
       3,
       4,
       6,
       3,
       6,
       7
    ],
    "850413-003":[
       5,
       9,
       5,
       4,
       4,
       6,
       6,
       4,
       5,
       4,
       9,
       5,
       4,
       5,
       8,
       4,
       5,
       7
    ],
    "750605-006":[
       4,
       6,
       5,
       5,
       9,
       9,
       5,
       4,
       9,
       5,
       9,
       5,
       5,
       4,
       5,
       4,
       4,
       5
    ],
    "870416-045":[
       5,
       6,
       4,
       5,
       9,
       6,
       5,
       4,
       4,
       3,
       4,
       6,
       4,
       4,
       6,
       5,
       3,
       10
    ],
    "921224-011":[
       8,
       10,
       4,
       5,
       6,
       7,
       6,
       3,
       8,
       8,
       5,
       6,
       8,
       4,
       5,
       4,
       7
    ],
    "830209-016":[
       5,
       8,
       4,
       7,
       6,
       9,
       9,
       8,
       6,
       8,
       5,
       4,
       5,
       8,
       7,
       2,
       3,
       6
    ]
 },
 2:{
    "871022-011":[],
    "860406-007":[],
    "890312-007":[],
    "880727-002":[],
    "870608-012":[],
    "881025-025":[],
    "860426-034":[],
    "850413-003":[],
    "750605-006":[],
    "870416-045":[],
    "921224-011":[],
    "830209-016":[]
 }
}
