export const rounds = [
  {
    name: 'Saturday foursomes',
    courseName: 'Torreby',
    type: 'foursome'
  },
  {
    name: 'Saturday four-ball',
    courseName: 'Sotenäs',
    type: 'four-ball'
  },
  {
    name: 'Sunday singles',
    courseName: 'Torreby',
    type: 'single'
  }
]

export const matches = [
  {
    round: 0,
    type: 'foursome',
    teamOneMembers: [
      '870608-012+871022-011'
    ],
    teamTwoMembers: [
      '890312-007+850413-003'
    ]
  },
  {
    round: 0,
    type: 'foursome',
    teamOneMembers: [
      '860406-007+860701-026'
    ],
    teamTwoMembers: [
      '880727-002+861214-017'
    ]
  },
  {
    round: 1,
    type: 'four-ball',
    teamOneMembers: [
      '870608-012',
      '860701-026'
    ],
    teamTwoMembers: [
      '890312-007',
      '850413-003'
    ]
  },
  {
    round: 1,
    type: 'four-ball',
    teamOneMembers: [
      '871022-011',
      '860406-007'
    ],
    teamTwoMembers: [
      '880727-002',
      '861214-017'
    ]
  },
  {
    round: 2,
    type: 'single',
    teamOneMembers: [
      '860701-026'
    ],
    teamTwoMembers: [
      '890312-007'
    ]
  },
  {
    round: 2,
    type: 'single',
    teamOneMembers: [
      '871022-011'
    ],
    teamTwoMembers: [
      '850413-003'
    ]
  },
  {
    round: 2,
    type: 'single',
    teamOneMembers: [
      '870608-012'
    ],
    teamTwoMembers: [
      '861214-017'
    ]
  },
  {
    round: 2,
    type: 'single',
    teamOneMembers: [
      '860406-007'
    ],
    teamTwoMembers: [
      '880727-002'
    ]
  }
]

export const players = [
  {
    id: "890312-007",
    firstName: "Daniel",
    lastName: "Asplund",
    hcp: 5.1,
  },
  {
    id: "880727-002",
    firstName: "Martin",
    lastName: "Hellgren",
    hcp: 4.7,
  },
  {
    id: "860701-026",
    firstName: "Sebastian",
    lastName: "Sporrong",
    hcp: 4.7,
  },
  {
    id: "860406-007",
    firstName: "Peter",
    lastName: "Wintzell",
    hcp: 8.6,
  },
  {
    id: "870608-012",
    firstName: "Henrik",
    lastName: "Åberg",
    hcp: 11.6,
  },
  {
    id: "850413-003",
    firstName: "Mattias",
    lastName: "Altsäter",
    hcp: 11.1,
  },
  {
    id: "871022-011",
    firstName: "Carl",
    lastName: "Wålinder",
    hcp: 9.7,
  },
  {
    id: "861214-017",
    firstName: "Rickard",
    lastName: "Berlin",
    hcp: 12.0,
  },
];

export const courses = {
  'Sotenäs': [
    {
      par: 4,
      hcp: 7
    },
    {
      par: 3,
      hcp: 13
    },
    {
      par: 5,
      hcp: 3
    },
    {
      par: 5,
      hcp: 9
    },
    {
      par: 3,
      hcp: 15
    },
    {
      par: 5,
      hcp: 5
    },
    {
      par: 4,
      hcp: 11
    },
    {
      par: 3,
      hcp: 17
    },
    {
      par: 4,
      hcp: 1
    },
    {
      par: 4,
      hcp: 18
    },
    {
      par: 5,
      hcp: 4
    },
    {
      par: 3,
      hcp: 2
    },
    {
      par: 4,
      hcp: 16
    },
    {
      par: 3,
      hcp: 14
    },
    {
      par: 4,
      hcp: 6
    },
    {
      par: 4,
      hcp: 12
    },
    {
      par: 5,
      hcp: 10
    },
    {
      par: 4,
      hcp: 8
    }
  ],
  'Torreby': [
    {
      par: 4,
      hcp: 3
    },
    {
      par: 4,
      hcp: 7
    },
    {
      par: 4,
      hcp: 1
    },
    {
      par: 5,
      hcp: 11
    },
    {
      par: 3,
      hcp: 17
    },
    {
      par: 4,
      hcp: 9
    },
    {
      par: 4,
      hcp: 15
    },
    {
      par: 5,
      hcp: 13
    },
    {
      par: 3,
      hcp: 5
    },
    {
      par: 3,
      hcp: 18
    },
    {
      par: 5,
      hcp: 8
    },
    {
      par: 3,
      hcp: 4
    },
    {
      par: 4,
      hcp: 2
    },
    {
      par: 5,
      hcp: 16
    },
    {
      par: 4,
      hcp: 12
    },
    {
      par: 3,
      hcp: 14
    },
    {
      par: 4,
      hcp: 6
    },
    {
      par: 4,
      hcp: 10
    }
  ]
}

export const score = {
    "0":{
       "870608-012+871022-011":[
          4,
          4,
          5,
          5,
          3,
          4,
          4,
          5,
          3,
          3,
          5,
          3,
          5,
          5,
          4,
          3,
          4
       ],
       "890312-007+850413-003":[
          5,
          5,
          4,
          5,
          3,
          4,
          4,
          5,
          3,
          3,
          5,
          3,
          4,
          5,
          4,
          4,
          6
       ],
       "860406-007+860701-026":[
          
       ],
       "880727-002+861214-017":[
          
       ]
    },
    "1":{
       "871022-011":[
          1,
          1,
          1,
          5,
          3,
          5,
          4,
          3,
          5,
          4,
          6,
          4,
          4,
          3,
          4,
          4
       ],
       "860406-007":[
          1,
          1,
          1,
          5,
          3,
          5,
          4,
          3,
          5,
          4,
          6,
          4,
          4,
          3,
          4,
          4
       ],
       "860701-026":[
          3,
          3,
          5,
          5,
          3,
          5,
          4,
          3,
          4,
          4,
          5,
          3,
          4,
          3,
          4,
          4,
          5
       ],
       "890312-007":[
          1,
          1,
          5,
          5,
          3,
          5,
          4,
          3,
          4,
          4,
          5,
          3,
          4,
          3,
          4,
          4,
          5
       ],
       "880727-002":[
          3,
          3,
          3,
          5,
          3,
          5,
          4,
          3,
          4,
          4,
          5,
          3,
          4,
          3,
          4,
          4
       ],
       "870608-012":[
          3,
          3,
          6,
          5,
          3,
          6,
          4,
          3,
          5,
          4,
          6,
          4,
          4,
          3,
          5,
          4,
          5
       ],
       "850413-003":[
          1,
          1,
          6,
          5,
          3,
          6,
          4,
          3,
          5,
          4,
          6,
          4,
          4,
          3,
          5,
          4,
          5
       ],
       "861214-017":[
          3,
          3,
          3,
          5,
          3,
          6,
          4,
          3,
          5,
          4,
          6,
          4,
          4,
          3,
          5,
          4
       ]
    },
    "2":{
       "871022-011":[
          5,
          5,
          5,
          6,
          3,
          5,
          4,
          6,
          3,
          3,
          5,
          4,
          4,
          6,
          5,
          4,
          5
       ],
       "860406-007":[
          4,
          5,
          7,
          5,
          3,
          4,
          5,
          6,
          3,
          4,
          7,
          4,
          5,
          6,
          4,
          3,
          4
       ],
       "860701-026":[
          5,
          4,
          4,
          7,
          3,
          5,
          5,
          4,
          3,
          5,
          5,
          4,
          4,
          5,
          4,
          4,
          5,
          3
       ],
       "890312-007":[
          4,
          4,
          5,
          5,
          4,
          5,
          5,
          6,
          3,
          3,
          6,
          3,
          4,
          6,
          5,
          4,
          4,
          4
       ],
       "880727-002":[
          5,
          4,
          6,
          6,
          4,
          4,
          6,
          6,
          3,
          4,
          5,
          3,
          4,
          5,
          5,
          4,
          4
       ],
       "870608-012":[
          5,
          5,
          5,
          6,
          4,
          5,
          5,
          6,
          3,
          3,
          6,
          3,
          5,
          5,
          7,
          3,
          6
       ],
       "850413-003":[
          4,
          6,
          4,
          6,
          4,
          3,
          5,
          7,
          3,
          3,
          5,
          5,
          4,
          6,
          5,
          4,
          6
       ],
       "861214-017":[
          5,
          5,
          5,
          7,
          3,
          6,
          6,
          7,
          4,
          3,
          6,
          5,
          5,
          6,
          4,
          5,
          7
       ]
    }
}
