export const getPlayer = (id, players) => {
  for (let player of players) {
    if (player.id === id) {
      return Object.assign({}, player)
    }
  }
}

export const getBothPlayersIfFoursome = (idList) => {
  let players = []
  for (let id of idList) {
    if (id.includes('+')) {
      players.push(id.split('+')[0])
      players.push(id.split('+')[1])
    } else {
      players.push(id)
    }
  }
  return players
}

export const getPlayerOrFoursomePlayer = (id, players) => {
  if (!id.includes('+')) {
    let player = getPlayer(id, players)
    return player
  }
  let playerIds = getBothPlayersIfFoursome([id])
  let playerOne = getPlayer(playerIds[0], players)
  let playerTwo = getPlayer(playerIds[1], players)
  let combinedPlayer = {
    id: id,
    hcp: Math.round((playerOne.hcp + playerTwo.hcp) * 10 / 2) / 10
  }
  return combinedPlayer
}

export const getInitials = (idList, playersObject) => {
  let players = getBothPlayersIfFoursome([idList])
  let initials = ''
  for (let index = 0; index < players.length; index++) {
    let playerObject = getPlayer(players[index], playersObject)
    if (index > 0) {
      initials += '+'
    }
    initials += playerObject.firstName.charAt(0) + playerObject.lastName.charAt(0)
  }
  return initials
}
