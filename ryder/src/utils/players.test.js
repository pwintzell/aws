import React from 'react'
import ReactDOM from 'react-dom'
import * as playerUtil from './players'

test('get foursome players from combined id 860406-007+870608-012 to ["860406-007, 870608-012]', () => {
  let listWithCombinedId = ['860401-007+870608-012']
  let resultingArray = ['860401-007', '870608-012']
  expect(playerUtil.getBothPlayersIfFoursome(listWithCombinedId)).toEqual(resultingArray)
})

test('get single player handicap', () => {
  let playerId = '860101-001'
  let players = [
    {
      id: '860101-001',
      hcp: 10
    }
  ]
  let expectedPlayer = {
    id: '860101-001',
    hcp: 10
  }
  expect(playerUtil.getPlayerOrFoursomePlayer(playerId, players, false)).toEqual(expectedPlayer)
})

test('get bestball player handicap', () => {
  let playerId = '860101-001'
  let players = [
    {
      id: '860101-001',
      hcp: 10
    }
  ]
  let expectedPlayer = {
    id: '860101-001',
    hcp: 10
  }
  expect(playerUtil.getPlayerOrFoursomePlayer(playerId, players, true)).toEqual(expectedPlayer)
})
