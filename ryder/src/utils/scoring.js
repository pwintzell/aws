import * as playersUtil from '../utils/players'

export const getHoleScore = (player, hole, round, score) => {
  return score[round][player][hole]
}

const getStrokesOnHole = (hole, hcp) => {
  let roundedHcp = Math.round(hcp)
  let strokesOnHole = Math.floor(roundedHcp / 18)
  let remainingHcp = roundedHcp - Math.floor(roundedHcp / 18) * 18
  if (hole.hcp <= remainingHcp) {
    strokesOnHole += 1
  }
  return strokesOnHole
}

export const getSuggestedHoleScore = (hole, hcp) => {
  return hole.par + getStrokesOnHole(hole, hcp)
}

export const getSuggestedScore = (holes, player) => {
  let suggestedScore = []
  for (let hole of holes) {
    suggestedScore.push(getSuggestedHoleScore(hole, player.hcp))
  }
  return suggestedScore
}

export const getSuggestedScores = (rounds, courses, matches, players) => {
  let results = {}
  for (let i = 0; i < rounds.length; i++) {
    results[i] = {}
  }
  for (let match of matches) {
    let round = match.round
    let courseName = rounds[round].courseName
    let course = courses[courseName]
    let playerIds = match.teamOneMembers.concat(match.teamTwoMembers)
    let playerObjects = [];
    let bestHcp = Infinity;
    const isBestBall = (playerIds.length === 4)
    for (let playerId of playerIds) {
      let playerObject = playersUtil.getPlayerOrFoursomePlayer(playerId, players)
      playerObjects.push(playerObject);
      bestHcp = Math.min(bestHcp, playerObject.hcp);
    }
    for (let playerObject of playerObjects) {
      const { id, hcp } = playerObject;
      const hcpDifference = hcp - bestHcp;
      const factor = isBestBall ? 0.9 : 1;
      const adjustedHcp = hcpDifference * factor;
      const playerObjectWithAdjustedHcp = Object.assign(
        playerObject,
        {
          hcp: adjustedHcp
        }
      );
      results[round][id] = getSuggestedScore(course, playerObjectWithAdjustedHcp)
    }
  }
  return results
}

/* Test driven här, behöver räkna ut och det suhuger */

const getPlayerNetScore = (playerId, round, score, holeNumber, hole, players, isBestBall, ballBestHcp) => {
  const playerObject = playersUtil.getPlayerOrFoursomePlayer(playerId, players, isBestBall)
  const factor = isBestBall ? 0.9 : 1;
  const netScore = score[round][playerId][holeNumber] - getStrokesOnHole(hole, (playerObject.hcp - ballBestHcp) * factor);
  return netScore
}

const getTeamNetScore = (teamMembers, round, score, holeNumber, hole, players, opponentTeamMembers) => {
  let ballBestHcp = Infinity;
  for (let playerId of [...teamMembers, ...opponentTeamMembers]) {
    let playerObject = playersUtil.getPlayerOrFoursomePlayer(playerId, players)
    ballBestHcp = Math.min(ballBestHcp, playerObject.hcp);
  }

  let playerId = teamMembers[0]
  let isBestBall = teamMembers.length > 1
  let teamScore = getPlayerNetScore(playerId, round, score, holeNumber, hole, players, isBestBall, ballBestHcp)
  if (isBestBall) {
    playerId = teamMembers[1]
    teamScore = Math.min(teamScore, getPlayerNetScore(playerId, round, score, holeNumber, hole, players, isBestBall, ballBestHcp))
  }
  return teamScore
}

export const getHoleResults = (score, match, holeNumber, hole, players) => {
  const { teamOneMembers, teamTwoMembers, round } = match;
  let teamOneNetScore = getTeamNetScore(teamOneMembers, round, score, holeNumber, hole, players, teamTwoMembers)
  let teamTwoNetScore = getTeamNetScore(teamTwoMembers, round, score, holeNumber, hole, players, teamOneMembers)
  if (teamOneNetScore > teamTwoNetScore) {
    return 'EUR'
  } else if (teamOneNetScore < teamTwoNetScore) {
    return 'USA'
  } else if (isNaN(teamOneNetScore) || isNaN(teamTwoNetScore)) {
    return 'NOT_PLAYED'
  }
  return 'EVEN'
}

export const getMatchResults = (score, match, holes, players) => {
  let results = []
  let usaWins = 0
  let eurWins = 0
  let notPlayedCount = 0
  for (let holeNumber = 0; holeNumber < holes.length; holeNumber++) {
    let result = getHoleResults(score, match, holeNumber, holes[holeNumber], players)
    if (result === 'USA') {
      usaWins++
    } else if (result === 'EUR') {
      eurWins++
    }
    if (Math.abs(usaWins - eurWins) > holes.length - holeNumber + notPlayedCount) {
      results.push('DEAD')
    } else {
      results.push(result)
    }
    if (result === 'NOT_PLAYED') {
      notPlayedCount++
    }
  }
  return results
}

export const getMatchesResults = (score, matches, rounds, courses, players) => {
  console.log('getMatchesResults')
  console.log(score)
  let matchResults = {}
  for (let round in rounds) {
    matchResults[round] = []
  }
  for (let i = 0; i < matches.length; i++) {
    let match = matches[i]
    matchResults[match.round].push(getMatchResults(score, match, courses[rounds[match.round].courseName], players))
  }
  return matchResults
}
