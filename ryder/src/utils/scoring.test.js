import React from 'react'
import ReactDOM from 'react-dom'
import * as scoringUtil from './scoring'

const testCourse = [
  {
    par: 4,
    hcp: 15
  },
  {
    par: 5,
    hcp: 7
  },
  {
    par: 4,
    hcp: 9
  },
  {
    par: 4,
    hcp: 5
  },
  {
    par: 4,
    hcp: 1
  },
  {
    par: 5,
    hcp: 11
  },
  {
    par: 4,
    hcp: 3
  },
  {
    par: 3,
    hcp: 17
  },
  {
    par: 4,
    hcp: 13
  },
  {
    par: 3,
    hcp: 14
  },
  {
    par: 4,
    hcp: 12
  },
  {
    par: 4,
    hcp: 2
  },
  {
    par: 4,
    hcp: 6
  },
  {
    par: 3,
    hcp: 10
  },
  {
    par: 5,
    hcp: 4
  },
  {
    par: 3,
    hcp: 8
  },
  {
    par: 4,
    hcp: 18
  },
  {
    par: 5,
    hcp: 16
  }
]

const scratchPlayer = {
  hcp: 0
}

const fiveHandicapper = {
  hcp: 4.7
}

const twentyHandicapper = {
  hcp: 20
}

const fiftyHandicapper = {
  hcp: 50.1
}

test('get score of scratch player on one hole', () => {
  let scoreResult = 5
  let hole = {
    par: 5,
    hcp: 2
  }
  let suggestedHoleScore = scoringUtil.getSuggestedHoleScore(hole, scratchPlayer.hcp)
  expect(scoreResult).toEqual(suggestedHoleScore)
})

test('get score of fiveHandicapper on one hole', () => {
  let scoreResult = 5
  let hole = {
    par: 5,
    hcp: 6
  }
  let suggestedHoleScore = scoringUtil.getSuggestedHoleScore(hole, fiveHandicapper.hcp)
  expect(scoreResult).toEqual(suggestedHoleScore)
})

test('get score of fiveHandicapper on one hole', () => {
  let scoreResult = 6
  let hole = {
    par: 5,
    hcp: 5
  }
  let suggestedHoleScore = scoringUtil.getSuggestedHoleScore(hole, fiveHandicapper.hcp)
  expect(scoreResult).toEqual(suggestedHoleScore)
})

test('get score of twentyHandicapper on one hole', () => {
  let scoreResult = 7
  let hole = {
    par: 5,
    hcp: 2
  }
  let suggestedHoleScore = scoringUtil.getSuggestedHoleScore(hole, twentyHandicapper.hcp)
  expect(scoreResult).toEqual(suggestedHoleScore)
})

test('get score of twentyHandicapper on one hole', () => {
  let scoreResult = 5
  let hole = {
    par: 4,
    hcp: 15
  }
  let suggestedHoleScore = scoringUtil.getSuggestedHoleScore(hole, twentyHandicapper.hcp)
  expect(scoreResult).toEqual(suggestedHoleScore)
})

test('get score of fiftyHandicapper on one hole', () => {
  let scoreResult = 6
  let hole = {
    par: 3,
    hcp: 2
  }
  let suggestedHoleScore = scoringUtil.getSuggestedHoleScore(hole, fiftyHandicapper.hcp)
  expect(scoreResult).toEqual(suggestedHoleScore)
})

test('get total score of scratch player', () => {
  let scoreResult = 72
  let suggestedScore = scoringUtil.getSuggestedScore(testCourse, scratchPlayer)
  let suggestedScoreResult = suggestedScore.reduce((a, b) => a + b, 0)
  expect(suggestedScoreResult).toEqual(scoreResult)
})

test('get total score of fiveHandicapper', () => {
  let scoreResult = 77
  let suggestedScore = scoringUtil.getSuggestedScore(testCourse, fiveHandicapper)
  let suggestedScoreResult = suggestedScore.reduce((a, b) => a + b, 0)
  expect(suggestedScoreResult).toEqual(scoreResult)
})

test('get total score of twentyHandicapper', () => {
  let scoreResult = 92
  let suggestedScore = scoringUtil.getSuggestedScore(testCourse, twentyHandicapper)
  let suggestedScoreResult = suggestedScore.reduce((a, b) => a + b, 0)
  expect(suggestedScoreResult).toEqual(scoreResult)
})

test('get score of scratch player', () => {
  let scoreResult = testCourse.map((hole) => hole.par)
  let suggestedScore = scoringUtil.getSuggestedScore(testCourse, scratchPlayer)
  expect(suggestedScore).toEqual(scoreResult)
})

test('get score of fiveHandicapper', () => {
  let scoreResult = testCourse.map((hole) => {
    let score = hole.par
    if (hole.hcp <= Math.round(fiveHandicapper.hcp)) {
      score++
    }
    return score
  })
  let suggestedScore = scoringUtil.getSuggestedScore(testCourse, fiveHandicapper)
  expect(suggestedScore).toEqual(scoreResult)
})

let singleMatch = {
  round: 0,
  type: 'single',
  teamOneMembers: [
    '860101-001'
  ],
  teamTwoMembers: [
    '860101-002'
  ]
}

let players = [
  {
    id: '860101-001',
    hcp: 0
  },
  {
    id: '860101-002',
    hcp: 0
  },
  {
    id: '860101-003',
    hcp: 20
  },
  {
    id: '860101-004',
    hcp: 4
  }
]

let hole = {
  par: 4,
  hcp: 5
}

test('get hole result for even match with same hcp', () => {
  let score = {
    '0': {
      '860101-001': [
        4
      ],
      '860101-002': [
        4
      ]
    }
  }
  let holeNumber = 0
  let expectedResult = 'EVEN'
  let actualResult = scoringUtil.getHoleResults(score, singleMatch, holeNumber, hole, players)
  expect(expectedResult).toEqual(actualResult)
})

test('get hole result for first team win with same hcp', () => {
  let score = {
    '0': {
      '860101-001': [
        3
      ],
      '860101-002': [
        4
      ]
    }
  }
  let holeNumber = 0
  let expectedResult = 'USA'
  let actualResult = scoringUtil.getHoleResults(score, singleMatch, holeNumber, hole, players)
  expect(expectedResult).toEqual(actualResult)
})

test('get hole result for second team win with same hcp', () => {
  let score = {
    '0': {
      '860101-001': [
        5
      ],
      '860101-002': [
        4
      ]
    }
  }
  let holeNumber = 0
  let expectedResult = 'EUR'
  let actualResult = scoringUtil.getHoleResults(score, singleMatch, holeNumber, hole, players)
  expect(expectedResult).toEqual(actualResult)
})

let singleMatchTwo = {
  round: 0,
  type: 'single',
  teamOneMembers: [
    '860101-003'
  ],
  teamTwoMembers: [
    '860101-004'
  ]
}

test('get hole result for even with different hcp', () => {
  let score = {
    '0': {
      '860101-003': [ // 20
        5
      ],
      '860101-004': [ // 4
        4
      ]
    }
  }
  let holeNumber = 0
  let expectedResult = 'EVEN'
  let actualResult = scoringUtil.getHoleResults(score, singleMatchTwo, holeNumber, hole, players)
  expect(expectedResult).toEqual(actualResult)
})

test('get hole result for team one win with different hcp', () => {
  let score = {
    '0': {
      '860101-003': [ // 20
        4
      ],
      '860101-004': [ // 4
        4
      ]
    }
  }
  let holeNumber = 0
  let expectedResult = 'USA'
  let actualResult = scoringUtil.getHoleResults(score, singleMatchTwo, holeNumber, hole, players)
  expect(expectedResult).toEqual(actualResult)
})

test('get hole result for team two win with different hcp', () => {
  let score = {
    '0': {
      '860101-003': [ // 20
        5
      ],
      '860101-004': [ // 4
        3
      ]
    }
  }
  let holeNumber = 0
  let expectedResult = 'EUR'
  let actualResult = scoringUtil.getHoleResults(score, singleMatchTwo, holeNumber, hole, players)
  expect(expectedResult).toEqual(actualResult)
})

test('get match results for team one lead with different hcp', () => {
  let score = {
    '0': {
      '860101-003': [ // 20
        6, 10, 4, 5, 3, 4
      ],
      '860101-004': [ // 4
        5, 4, 3, 7, 3, 4
      ]
    }
  }
  let holes = [
    {
      par: 4,
      hcp: 1
    },
    {
      par: 4,
      hcp: 7
    },
    {
      par: 3,
      hcp: 18
    },
    {
      par: 5,
      hcp: 2
    },
    {
      par: 3,
      hcp: 14
    },
    {
      par: 4,
      hcp: 9
    },
    {
      par: 4,
      hcp: 1
    },
    {
      par: 4,
      hcp: 7
    },
    {
      par: 3,
      hcp: 18
    },
    {
      par: 5,
      hcp: 2
    },
    {
      par: 3,
      hcp: 14
    },
    {
      par: 4,
      hcp: 9
    },
    {
      par: 4,
      hcp: 1
    },
    {
      par: 4,
      hcp: 7
    },
    {
      par: 3,
      hcp: 18
    },
    {
      par: 5,
      hcp: 2
    },
    {
      par: 3,
      hcp: 14
    },
    {
      par: 4,
      hcp: 9
    }
  ]
  let expectedResult = [
    'EVEN',
    'EUR',
    'EUR',
    'USA',
    'USA',
    'USA',
    'NOT_PLAYED',
    'NOT_PLAYED',
    'NOT_PLAYED',
    'NOT_PLAYED',
    'NOT_PLAYED',
    'NOT_PLAYED',
    'NOT_PLAYED',
    'NOT_PLAYED',
    'NOT_PLAYED',
    'NOT_PLAYED',
    'NOT_PLAYED',
    'NOT_PLAYED'
  ]
  let actualResult = scoringUtil.getMatchResults(score, singleMatchTwo, holes, players)
  expect(expectedResult).toEqual(actualResult)
})

test('get match results where holes are dead', () => {
  let score = {
    '0': {
      '860101-003': [ // 20
        6, 10, 4, 5, 3, 5, 1, 1, 1, 1, 1, 1, 1, 1, 1
      ],
      '860101-004': [ // 4
        5, 4, 3, 7, 3, 4, 2, 2, 2, 2, 2, 2, 2, 2, 2
      ]
    }
  }
  let holes = [
    {
      par: 4,
      hcp: 1
    },
    {
      par: 4,
      hcp: 7
    },
    {
      par: 3,
      hcp: 18
    },
    {
      par: 5,
      hcp: 2
    },
    {
      par: 3,
      hcp: 14
    },
    {
      par: 4,
      hcp: 9
    },
    {
      par: 4,
      hcp: 1
    },
    {
      par: 4,
      hcp: 7
    },
    {
      par: 3,
      hcp: 18
    },
    {
      par: 5,
      hcp: 2
    },
    {
      par: 3,
      hcp: 14
    },
    {
      par: 4,
      hcp: 9
    },
    {
      par: 4,
      hcp: 1
    },
    {
      par: 4,
      hcp: 7
    },
    {
      par: 3,
      hcp: 18
    },
    {
      par: 5,
      hcp: 2
    },
    {
      par: 3,
      hcp: 14
    },
    {
      par: 4,
      hcp: 9
    }
  ]
  let expectedResult = [
    'EVEN',
    'EUR',
    'EUR',
    'USA',
    'USA',
    'EVEN',
    'USA',
    'USA',
    'USA',
    'USA',
    'USA',
    'USA',
    'DEAD',
    'DEAD',
    'DEAD',
    'DEAD',
    'DEAD',
    'DEAD'
  ]
  let actualResult = scoringUtil.getMatchResults(score, singleMatchTwo, holes, players)
  expect(expectedResult).toEqual(actualResult)
})

test('get match results where holes are dead with only one hole is dead', () => {
  let score = {
    '0': {
      '860101-001': [ // 20
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1
      ],
      '860101-002': [ // 4
        5, 5, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1
      ]
    }
  }
  let holes = [
    {
      par: 4,
      hcp: 1
    },
    {
      par: 4,
      hcp: 7
    },
    {
      par: 3,
      hcp: 18
    },
    {
      par: 5,
      hcp: 2
    },
    {
      par: 3,
      hcp: 14
    },
    {
      par: 4,
      hcp: 9
    },
    {
      par: 4,
      hcp: 1
    },
    {
      par: 4,
      hcp: 7
    },
    {
      par: 3,
      hcp: 18
    },
    {
      par: 5,
      hcp: 2
    },
    {
      par: 3,
      hcp: 14
    },
    {
      par: 4,
      hcp: 9
    },
    {
      par: 4,
      hcp: 1
    },
    {
      par: 4,
      hcp: 7
    },
    {
      par: 3,
      hcp: 18
    },
    {
      par: 5,
      hcp: 2
    },
    {
      par: 3,
      hcp: 14
    },
    {
      par: 4,
      hcp: 9
    }
  ]
  let expectedResult = [
    'USA',
    'USA',
    'EVEN',
    'EVEN',
    'EVEN',
    'EVEN',
    'EVEN',
    'EVEN',
    'EVEN',
    'EVEN',
    'EVEN',
    'EVEN',
    'EVEN',
    'EVEN',
    'EVEN',
    'EVEN',
    'EVEN',
    'DEAD'
  ]
  let actualResult = scoringUtil.getMatchResults(score, singleMatch, holes, players)
  expect(expectedResult).toEqual(actualResult)
})
