#!/bin/sh
sudo apt-get update
sudo apt-get -y install \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
sudo apt-get update
sudo apt-get -y install docker-ce
sudo apt-get -y install docker-compose
sudo usermod -aG docker ${USER}
sudo apt-get -y install apache2
sudo apt-get -y install php libapache2-mod-php
mkdir -p /home/ubuntu/repo
cd /home/ubuntu/repo
sudo chown www-data: /home/ubuntu/repo -R
sudo chown www-data: /var/www -R
sudo -H -u www-data bash -c 'git config --global credential.helper store'
sudo -H -u www-data bash -c 'git clone https://pwintzell@bitbucket.org/pwintzell/aws.git'
cd ..
cd ..
chmod +x /home/ubuntu/repo/aws/setup/rebuild.sh
sudo cp /home/ubuntu/repo/aws/setup/config/ports.conf /etc/apache2/ports.conf
sudo usermod -aG docker www-data
sudo service apache2 restart
sudo rm /var/www/html/index.html
sudo cp /home/ubuntu/repo/aws/setup/config/rebuild.php /var/www/html/rebuild.php
sudo chown www-data: . -R
sudo mkdir /var/log/wintzell/
sudo chown www-data: /var/log/wintzell/ -R
