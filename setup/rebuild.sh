#!/bin/bash
exec 3>&1 4>&2
trap 'exec 2>&4 1>&3' 0 1 2 3
exec 1>/var/log/wintzell/installing.log 2>&1
# Everything below will go to the file 'installing.out':
# TODO: add apt-get -y autoremove
# TODO: add apt-get -y autoclean
echo 'Cleaning up:'
docker system prune -f
cd /home/ubuntu/repo/aws/
cp /home/ubuntu/repo/aws/setup/config/rebuild.php /var/www/html/rebuild.php
export HOME=/var/www
echo 'Going to pull:'
git pull
docker-compose kill
cd nginx
echo 'Building docker image:'
docker build -t wintzell/nginx .
cd ..
cd ryder
echo 'Building docker image:'
docker build -t wintzell/ryder .
cd ..
cd megabowl
echo 'Building docker image:'
docker build -t wintzell/megabowl .
cd ..
cd ryder-preamble
echo 'Building docker image:'
docker build -t wintzell/ryder-preamble .
docker image prune -f
cd ..
cd ..
echo 'Running the new docker image(s):'
docker-compose -f /home/ubuntu/repo/aws/docker-compose.yml up -d